
<?php
	if($recaptchaAllowed->recaptchaAllowed == "1")
	{
		echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
	}
?>
<section class="login-block" style="background-image: url(<?php echo base_url('assets/')?>images/banner-slider1.jpg) !important;
    background-size: cover;
    background-repeat: no-repeat;">
  <div id="login">
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form name="frmLogin" id="frmLogin" class="form" action="<?php echo base_url('login/userlogin')?>" method="post">
                            <h1 class="text-center primary-text font-size-30">Login</h1><br>
                            <?php if($this->session->flashdata('message')){?>
                                <div class="alert alert-success">
                                    <strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
                                </div>
                                <?php } ?>
                                <?php if($this->session->flashdata('message_e')){?>
                                <div class="alert alert-warning">
                                    <strong>Error!</strong> <?php echo $this->session->flashdata('message_e');?>.
                                </div>
                                <?php } ?>
                            <div class="form-group">
                                <label for="username" class="primary-text paragraph-text2">Email Id:</label><br>
                                <input type="email" name="email" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="primary-text paragraph-text2">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                            <br>
                            <input type="hidden" class="recaptchaAllowed" name="recaptchaAllowed" value="<?php echo $recaptchaAllowed->recaptchaAllowed;?>">
						
                            
                            <div class="mb-xs text-center server_data_status">
                            
						    </div>
                            <div class=" mb-xs text-center">
                                <img src="<?php echo base_url('assets/')?>images/loader/loader.gif" class='img-responsive loading_img centered-loading-image' id='loading_img' alt='loading' style='width:80px; height:80px; display:none;'/>
                            </div>
                            <div class="form-group">
                                
                                 <button class="btn btn-outline-danger" id="recaptcha-submit"><i class="fas fa-sign-in-alt" style="font-size:14px"></i> Login</button><br>

                                <div class="text-center">
                                 <a href="<?php echo base_url('login/forgotpassword')?>" class="secondary-text paragraph-text2">Forgot Password ?</a>
                              </div>
                                <div id="register-link" class="text-center">
                                    <a href="<?php echo base_url('free-register')?>" class="secondary-text paragraph-text2">Don't have an Account ? Register here</a>
                                </div>
                            <?php
							if($recaptchaAllowed->recaptchaAllowed == "1")
							{
								echo "<div class='form-group'>
									<div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaAllowed->reCaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
								</div>";
							}
						?>
						
						<script></script>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
		function onSubmit(token)
		{
		    document.getElementById("frmLogin").submit();
		}

		function validate(event) 
		{
		    event.preventDefault();
		    var email = $('#email').val();
			var password = $('#password').val();
			var recaptchaAllowed = $('.recaptchaAllowed').val();
			var task = "login_client_user";

			/* Email Validation */
			if(email=='' || email==null)
	        {
	        	$('.server_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty! </strong> Enter email address.</div>");
	            return false;
	        }

	        /* Password validation */
	        if(password=='' || password==null)
	        {
	        	$('.server_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty! </strong> Enter your password.</div>");
	            return false;
	        }

	        //recaptcha validation 
	        if(recaptchaAllowed=='1')
			{
				grecaptcha.execute();
			}
			else
			{
				document.getElementById("frmLogin").submit();
			}

			$('.loading_img').show();
			$('.server_data_status').html("");
		}

		function onload() 
		{
		    var element = document.getElementById('recaptcha-submit');
		    element.onclick = validate;
		}
        onload();
	</script>