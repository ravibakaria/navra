<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Navra Bayko</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/bootstrap.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">
      <link href="https://fonts.googleapis.com/css?family=Cambo&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/datatables.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/style.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins|Roboto&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lora&display=swap" rel="stylesheet">
      <link rel="icon" type="image/png" href="<?php echo base_url('assets/') ?>images/heart-title.png" sizes="50x50">
      <script src="<?php echo base_url('assets/') ?>/js/jquery.min.js"></script>
      <script src="<?php echo base_url('assets/') ?>js/popper.min.js"></script>
      <script src="<?php echo base_url('assets/') ?>js/bootstrap.min.js"></script>
      <script src="<?php echo base_url('assets/') ?>js/datatables.min.js"></script>
   </head>
   <body>
      <div class="login-wrapper">
         <!-- primary header starts here -->
         <nav class="navbar navbar-expand-lg navbar-light">
            <div class="logo" style="width: 38%">
               <a class="navbar-brand" href="<?php echo base_url() ?>">
               <img src="<?php echo base_url('assets/') ?>images/logo.png" style="height:75px;">
               </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
               <div class="navbar-nav"> <a class="nav-item nav-link active paragraph-text2" href="advance-search.html">Advance Search <span class="sr-only">(current)</span></a>
                  <a class="nav-item nav-link font-weight-500 white-text paragraph-text2" href="<?php echo base_url('free-register')?>">Free Registration</a>
                  <a class="nav-item nav-link font-weight-500 white-text paragraph-text2" href="<?php echo base_url('users/Membership-Plans')?>">Membership Plans</a>
                  <a class="nav-item nav-link font-weight-500 white-text paragraph-text2" href="directory.html">Wedding Directory</a>
                  <?php if($this->session->userdata('user')!=''){?>
                  <a class="nav-item nav-link font-weight-500 white-text paragraph-text2" href="<?php echo base_url('user')?>">My Account</a>
                  <a class="nav-item nav-link font-weight-500 white-text paragraph-text2" href="<?php echo base_url('user/logout')?>">Logout</a>
                  <?php } else { ?>
                  <a class="nav-item nav-link font-weight-500 white-text paragraph-text2" href="<?php echo base_url('login')?>">Login</a>
                  <?php } ?>
               </div>
            </div>
         </nav>
         <!-- primary  header ends here -->