<?php
	if($recaptchaAllowed->recaptchaAllowed == "1")
	{
		echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
	}
?>
  <link href= 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css'rel='stylesheet'> 
      

      
    <script src= "https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script> 
<style>
.danger_error {
    border: 1px solid red !important;
}
</style>
<section class="register-block">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-9">

               <div class="register-block-box-shadow">
               <div class="row justify-content-center mb-5 sm-mb-3">
                  <h2 class="title divider mb-3"><strong>Register Now!</strong></h2>
                  <div class="col-md-12 text-center">
                     <img src="<?php echo base_url('assets/')?>images/divider.png">
                  </div>
               </div>
               <form name="frmLogin" id="frmLogin" autocomplete="off" method="post"  action="<?php echo base_url('login_action')?>">
                  <div class="panel-body register-panel-body">
                     <input type="hidden" class="MinPassLength" value="10">
                     <input type="hidden" class="MaxPassLength" value="24">
                     <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">First Name</label>
                              <input type="text" name="fname" id="fname" class="form-control fname" placeholder="Ex: John" value="">
                           </div>
                           <div class="fname_error"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">Last Name</label>
                              <input type="text" name="lname" id="lname" class="form-control lname" placeholder="Ex: Smith" value="">
                           </div>
                           <div class="lname_error"></div>
                        </div>
                     </div>

                     <div class="row">
                         <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">Gender</label>
                              <select class="form-control gender" id="gender" name="gender">
                                 <option value="0">Select Gender</option>
                                 <option value="1">Male</option>
                                 <option value="2">Female</option>
                              </select>
                           </div>
                           <div class="gender_error"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">Date of Birth</label>
                              <input type="text" name="dob" id="dob" class="form-control dob" placeholder="YYYY-MM-DD" value="">
                           </div>
                           <div class="dob_error"></div>
                        </div>
                     </div>

                        <div class="row">
                           <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">Email Id</label>
                              <input type="text" name="email" id="email" class="form-control email" placeholder="Ex: John@gmail.com" value="">
                           </div>
                           <div class="email_error"></div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">Mobile Number</label>
                              <div class="input-group">
                                 <span class="input-group-addon country_code">
                                 </span>
                                 <input type="text" name="phonenumber" class="form-control phonenumber" placeholder="Mobile Number" value="" maxlength="20">
                              </div>
                              <input type="hidden" name="country_phone_code" class="country_phone_code" value="">
                              <div class="phonenumber_error"></div>
                           </div>
                        </div>
                     </div>

                       
                     <div class="row">

                         
                           <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">Country</label>
                              <select name="country" class="form-control country">
                                 <option value="0">Select Country</option>
                                 <?php
                                       foreach ($countries as $row_country) 
                                       {
                                         $id = $row_country->id;
                                         $name = $row_country->name;
     
                                         echo "<option value='".$id."'>".$name."</option>";
                                       }
                                    ?>
                              </select>
                           </div>
                           <div class="country_error"></div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">State</label>
                              <select name="state" class="form-control state">
                              </select>
                           </div>
                           <div class="state_error"></div>
                        </div>
                     </div>

                        
                       <div class="row">
                        
                       <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">City</label>
                              <select name="city" class="form-control city">
                              </select>
                           </div>
                           <div class="city_error"></div>
                    </div>
                        
                           <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">Password</label>
                              <input type="password" name="password" class="form-control password" id="inputPassword" placeholder="Password" value="">
                           </div>
                           <div class="password_error"></div>
                        </div>
                     </div>
                     

                     <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group">
                              <label class="control-label primary-text">Confirm Password</label>
                              <input type="password" class="form-control confirm_password" name="confirm_password" id="inputPasswordConfirm" placeholder="Confirm Password" value="">
                           </div>
                           <div class="confirm_password"></div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-sm-12">
                           <div class="form-group">
                              <div class="password-notification">
                                 <p class=" primary-text"><b>Instructions to set password:</b><br>
                                    Password should contain atleast one uppercase character.<br>
                                    Password should contain atleast one digit.<br>
                                    Password should contain atleast one special character from !@#$%^*()_=+{}|;:,&lt;.&gt;
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                           <div class="form-group form-check">
                              <input type="checkbox" class="form-check-input" id="exampleCheck1">
                              <label class="form-check-label" for="exampleCheck1">I agree to the Terms Of Services & Privacy Policy.</label>
                           </div>
                           <div class="agree_check_error"></div>
                        </div>
                     </div>
                     <div class="form-group">
                     </div>
                     <input type="hidden" class="recaptchaAllowed" value="1">
                     <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div class="row">
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 centered-data">
                                 <img src="images/loader/loader.gif" class="img-responsive loading_img centered-loading-image" id="loading_img" alt="loader" style="width:60px; height:60px; display:none;">
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 server_data_status">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <footer class="panel-footer register-panel-footer centered-data">
                     <button id="recaptcha-submit" onclick="onload()" class="btn btn-outline-danger">Register</button>
                  </footer>
                  
                  <script></script>
                  <?php
							if($recaptchaAllowed->recaptchaAllowed == "1")
							{
								echo "<div class='form-group'>
									<div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaAllowed->reCaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
								</div>";
							}
						?>
               </form>
            </div>
         </div>
      </div>
   </div>
   <script>
      function onSubmit(token)
      {
         document.getElementById("frmLogin").submit();
      }

      function validate(event) 
      {
         event.preventDefault();
         var fname = $('.fname').val();
         var lname = $('.lname').val();
         var email = $('.email').val();
         var country = $('.country').val();
         var state = $('.state').val();
         var city = $('.city').val();
         var dob = $('.dob').val();
         var gender = $('.gender').val();
         var country_phone_code = $('.country_phone_code').val();
         var phonenumber = $('.phonenumber').val();
         var password = $('.password').val();
         var confirm_password = $('.confirm_password').val();
         var agree_check = $('#exampleCheck1').is(':checked');
         var MinPassLength = $('.MinPassLength').val();
         var MaxPassLength = $('.MaxPassLength').val();
         var recaptchaAllowed = $('.recaptchaAllowed').val();
         var empty_success=0;            
//alert(agree_check);
         if(MinPassLength=='0' || MinPassLength=='' || MinPassLength==null)
         {
               MinPassLength='10';
         }

         if(MaxPassLength=='0' || MaxPassLength=='' || MaxPassLength==null)
         {
               MaxPassLength='100';
         }

         /* First name Validation */
         if(fname=='' || fname==null)
         {
               $('.fname').addClass('danger_error');
               empty_success=empty_success+1;
         }


         /* Last name Validation */
         if(lname=='' || lname==null)
         {
               $('.lname').addClass('danger_error');
               empty_success=empty_success+1;
         }

         /* Date of Birth validation */
         if(dob=='' || dob==null)
         {
               $('.dob').addClass('danger_error');
               empty_success=empty_success+1;
         }
         /* Country validation */
         if(country=='0' || country=='' || country==null)
         {
               $('.country').addClass('danger_error');
               empty_success=empty_success+1;
         }

         /* State validation */
         if(state=='0' || state=='' || state==null)
         {
               $('.state').addClass('danger_error');
               empty_success=empty_success+1;
         }

         /* city validation */
         if(city=='0' || city=='' || city==null)
         {
               $('.city').addClass('danger_error');
               empty_success=empty_success+1;
         }


         /* Email Validation */
         if(email=='' || email==null)
         {
               $('.email').addClass('danger_error');
               empty_success=empty_success+1;
         }

         function validateEmail($email) {
               var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
               return emailReg.test( $email );
         }

         if(email!='' && !validateEmail(email)) 
         { 
               $('.email').focus();
               $('.email_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Enter valid Email.</div>");
               $('.email').addClass('danger_error');
               empty_success=empty_success+1;
         }


         /* Gender validation */
         if(gender=='0')
         {
               $('.gender').addClass('danger_error');
               empty_success=empty_success+1;
         }

         /* Phone number validation */
         if(phonenumber=='' || phonenumber==null)
         {
               $('.phonenumber').addClass('danger_error');
               empty_success=empty_success+1;
         }

         if(phonenumber!='' && phonenumber.length<4)
         {
               $('.phonenumber').focus();
               $('.phonenumber_error').show();
               $('.phonenumber_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Mobile number should be of minimum 4 digits!</div>");
               $('.phonenumber').addClass('danger_error');
               empty_success=empty_success+1;
         }

         /* Password validation */
         if(password=='' || password==null)
         {
               $('.password').addClass('danger_error');
               empty_success=empty_success+1;
         }

         if(password!='' && password.length < MinPassLength)
         {
               $('.password').focus();
               $('.password_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Password should be of minimum "+MinPassLength+" characters long!</div>");
               $('.password').addClass('danger_error');
               empty_success=empty_success+1;
         }

         if(password!='' && password.length > MaxPassLength)
         {
               $('.password').focus();
               $('.password_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Password length not exceed more than "+MaxPassLength+" !</div>");
               $('.password').addClass('danger_error');
               empty_success=empty_success+1;
         }

         /* Confirm Password validation */
         if(confirm_password=='' || confirm_password==null)
         {
               $('.confirm_password').addClass('danger_error');
               empty_success=empty_success+1;
         }

         if(confirm_password!='' && confirm_password != password)
         {
               $('.confirm_password').focus();
               $('.confirm_password_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Error: </strong> Password & Confirm password not same!</div>");
               $('.confirm_password').addClass('danger_error');
               empty_success=empty_success+1;
         }

         /* Agreement check validation */
         if(agree_check==false)
         {
               $('.agree_check_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Emty: </strong> Please check the terms & condtions.</div>");
               $('#agree_check').addClass('danger_error');
               empty_success=empty_success+1;
         }

         if(empty_success>0)
         {
               $('.server_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><span class='fa fa-exclamation-circle'></span><strong> Empty! </strong> Please enter all required field & then click on register button.</div>");
               return false;
         }

         //recaptcha validation 
         if(recaptchaAllowed=='1')
         {
               grecaptcha.execute();
         }
         else
         {
               document.getElementById("frmLogin").submit();
         }

         $('.loading_img').show();
         $('.server_data_status').html("");
         $('.error_status').html("");
         $('.agree_check_error').html("");
      }

      function onload() 
      {
         var element = document.getElementById('recaptcha-submit');
         element.onclick = validate;
      }
      onload();



      $(document).on('change', '.country', function() {
      var state = this.value;
         $.ajax({
            url: "<?php echo base_url('Home/state');?>",
            method: "POST",
            data: {state: state},
            cache: true,
            success:function(data){
            
            data = $.parseJSON(data);
           // console.log(data);
            var i;
            var html='';
            html+='<option value="all">Select State </option>';
            for (i = 0; i < data.city.length; i++) {
            html += "<option value ='"+ data.city[i].id+"'>"+data.city[i].name+"</option>";
            }
            $(".state").html(html);
            $(".country_phone_code").val(data.code.phonecode);
            $(".country_code").html(data.code.phonecode);
           
            
         }
            

         }).fail(function(data){
            console.log(data);
         });

      });
      $(document).on('change', '.state', function() {
      var state = this.value;
         $.ajax({
            url: "<?php echo base_url('Home/getcity');?>",
            method: "POST",
            data: {state: state},
            cache: true,
            success:function(data){
            
            data = $.parseJSON(data);
            var i;
            var html='';
            html+='<option value="all">Select City </option>';
            for (i = 0; i < data.length; i++) {
               html += "<option value ='"+ data[i].id+"'>"+data[i].name+"</option>";
            }
            $(".city").html(html);
           
            
         }
            

         }).fail(function(data){
            console.log(data);
         });

      });

      $(document).ready(function() { 
          
          $(function() { 
              $( "#dob" ).datepicker({
               dateFormat: 'yy-mm-dd', 
               changeMonth: true, 
               changeYear: true,
               maxDate:'-18y', 
              
              }); 
          }); 
      }) 
    </script>