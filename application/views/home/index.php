
<div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
            <!-- Carousel indicators -->
            <ol class="carousel-indicators">
               <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
               <li data-target="#myCarousel" data-slide-to="1"></li>
               <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for carousel items -->
            <div class="carousel-inner">
               <div class="carousel-item active">
                  <img src="<?php echo base_url('assets/') ?>images/banner-slider1.jpg" alt="Second Slide">
                  <div class="carousel-caption d-none d-md-block">
                     <h1 class="font-size-50">Navra Bayko</h1>
                     <h2>Best Matrimonial Website</h2>
                     <p class="font-size-24">Find someone special you are looking for</p>
                     <div class="row">
                        <div class="btn-center">
                           <a href="#">
                           <button type="button" class="btn btn-outline-danger"><i class='far fa-address-card' style='font-size:16px'></i>&nbsp;&nbsp;Create Profile</button>
                           </a>&nbsp;&nbsp;
                           <a href="#">
                           <button type="button" class="btn btn-danger"><i class="fa fa-search" style='font-size:16px'></i>&nbsp;&nbsp;Search Profile</button>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <img src="<?php echo base_url('assets/') ?>images/banner-slider2.jpg" alt="Second Slide">
                  <div class="carousel-caption d-none d-md-block">
                     <h1 class="font-size-50">Navra Bayko</h1>
                     <h2>Best Matrimonial Website</h2>
                     <p class="font-size-24">Find someone special you are looking for</p>
                     <div class="row">
                        <div class="btn-center">
                           <button type="button" class="btn btn-outline-danger"><i class='far fa-address-card' style='font-size:14px'></i>&nbsp;&nbsp;Create Profile</button>&nbsp;&nbsp;
                           <button type="button" class="btn btn-danger"><i class="fa fa-search" style='font-size:14px'></i>&nbsp;&nbsp;Search Profile</button>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <img src="<?php echo base_url('assets/') ?>images/banner-slider3.jpg" alt="Second Slide">
                  <div class="carousel-caption d-none d-md-block">
                     <h1 class="font-size-50">Navra Bayko</h1>
                     <h2>Best Matrimonial Website</h2>
                     <p class="font-size-24">Find someone special you are looking for</p>
                     <div class="row">
                        <div class="btn-center">
                           <button type="button" class="btn btn-outline-danger"><i class='far fa-address-card' style='font-size:14px'></i>&nbsp;&nbsp;Create Profile</button>&nbsp;&nbsp;
                           <button type="button" class="btn btn-danger"><i class="fa fa-search" style='font-size:14px'></i>&nbsp;&nbsp;Search Profile</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Carousel controls -->
            <a class="carousel-control-prev" href="#myCarousel" data-slide="prev"> <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#myCarousel" data-slide="next"> <span class="carousel-control-next-icon"></span>
            </a>
         </div>
         <!-- homepage slider ends here -->
         <!-- Homepage form start here -->
         <div class="search-form-wrapper">
            <div class="container">
               <form>
                  <div class="form-row">
                     <div class="inline-form-search">
                        <label class="searching-for"><b>Looking for: </b></label>
                        <div class="switch-field homepage-gender-search">
                           <input class="gender" type="radio" id="1" name="gender" value="1" checked="">
                           <label for="1"><i class="fa fa-check gender-check_1"></i> Groom  </label><input class="gender" type="radio" id="2" name="gender" value="2">
                           <label for="2"> <i class="fa fa-check gender-check_2" style="display:none;"></i> Bride </label>                              <input type="hidden" class="count_gender" value="2">
                        </div>
                     </div>
                     &nbsp;&nbsp;
                     <div class="form-group col-md-3 col-6">
                        <div class="ageWrap">
                           <label class="label white-text paragraph-text2">Age</label>&nbsp;&nbsp;
                           <div class="ageFrom">
                              <div class="Dropdown-root">
                                 <select name="" id="" class="Dropdown-control is-selected">
                                       <?php
                                       for($i=18;$i<100;$i++)
                                       {
                                          echo "<option class='home-search-filter' value='".$i."'>".$i."</option>";
                                       }
                                    ?>
                                    </select>  
                              </div>
                           </div>
                           &nbsp;&nbsp;
                           <div class="ageTo white-text paragraph-text2">to</div>
                           &nbsp;&nbsp;
                           <div class="ageUpto">
                              <div class="Dropdown-root">
                              <select name="" id="" class="Dropdown-control is-selected">
                                       <?php
                                       for($i=18;$i<100;$i++)
                                       {
                                          echo "<option class='home-search-filter' value='".$i."'>".$i."</option>";
                                       }
                                    ?>
                                    </select>  
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-md-3 col-6">
                        <div class="ageWrap">
                           <label class="label white-text paragraph-text2">Country&nbsp;&nbsp;</label>
                           <div class="ageFrom">
                              <div class="Dropdown-root">
                              <select name="" id="" class="Dropdown-control is-selected">
                                       <?php
                                       foreach ($countries as $row_country) 
                                       {
                                         $id = $row_country->id;
                                         $name = $row_country->name;
     
                                         echo "<option value='".$id."'>".$name."</option>";
                                       }
                                    ?>
                                    </select>  
                              </div>
                           </div>
                        </div>
                     </div>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 inline-form-search">
                                    <button class="search-btn" id="hero-search" name="search">Search</button>
                                 </div>
                  </div>
               </form>
            </div>
         </div>
         <br>
         <!-- Homepage form ends here -->
         <!-- steps section starts here -->
         <section class="page-section-ptb text-center">
            <div class="container-fluid">
               <div class="row justify-content-center mb-5 sm-mb-3">
                  <div class="col-md-8">
                     <h2 class="title divider mb-3"><strong>Finding someone special has never been so easy...</strong></h2>
                     <p class="lead">We love bringing people together and we have helped thousands of singles find thier love</p>
                  </div>
                  <div class="col-md-12">
                     <img src="<?php echo base_url('assets/') ?>images/divider.png">
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-3">
                     <div class="timeline-badge mb-2">
                        <img class="img-center" src="<?php echo base_url('assets/') ?>images/create-profile-primary-icon.jpg" alt="">
                     </div>
                     <h3 class="title divider-3 mb-3">CREATE PROFILE</h3>
                     <p class="paragraph-text2">A compelling profile is key to success and adding fresh photos will ensure better matrimonial success</p>
                  </div>
                  <div class="col-md-3">
                     <div class="timeline-badge mb-2">
                        <img class="img-center" src="<?php echo base_url('assets/') ?>images/find-match-primary-icon.jpg" alt="">
                     </div>
                     <h3 class="title divider-3 mb-3">FIND MATCH</h3>
                     <p class="paragraph-text2">Browse through thousands of amazing profiles & find someone special you are looking for life-partner</p>
                  </div>
                  <div class="col-md-3">
                     <div class="timeline-badge mb-2">
                        <img class="img-center" src="<?php echo base_url('assets/') ?>images/start-interacting-primary-icon.jpg" alt="">
                     </div>
                     <h3 class="title divider-3 mb-3">START INTERACTING</h3>
                     <p class="paragraph-text2">Carefully exchange your contact details, get to know each other & start meeting your most preferred match</p>
                  </div>
                  <div class="col-md-3">
                     <div class="timeline-badge mb-2">
                        <img class="img-center" src="<?php echo base_url('assets/') ?>images/happy-marriege-primary-icon.jpg" alt="">
                     </div>
                     <h3 class="title divider-3 mb-3">HAPPY MARRIAGE</h3>
                     <p class="paragraph-text2">We wish you & your life-partner a very happy married life. Please do share your success story with us.</p>
                  </div>
               </div>
            </div>
         </section>
         <br>
         <br>
         <!-- steps section starts here -->
         <!-- featured profile starts here -->
         <section>
      <div class="container-fluid" style="background: #f5f5f5; padding: 20px;">
        <div class="container text-center">
          <h2 class="title divider mb-3"><strong>Featured Profiles</strong></h2>
          <p class="lead">Some of our featured member profiles...</p>
          <div class="col-md-12">
            <img src="<?php echo base_url('assets/')?>images/divider.png">
          </div>
        </div>
        <br>
        <div class="products-box">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="thumb-pad2 maxheight">
                  <div class="box_inner">
                    <div class="thumbnail">
                      <figure>
                        <img src="<?php echo base_url('assets/')?>images/recent-profile3.jpg" alt="">
                      </figure>
                      <div class="caption"> <a href="#"><strong>Maya Joshi, From Mumbai</strong></a>
                        <p>Age: 20
                          <br>Profession: Self Employed
                          <br>Religion: Hindu
                          <br>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="thumb-pad2 maxheight">
                  <div class="box_inner">
                    <div class="thumbnail">
                      <figure>
                        <img src="<?php echo base_url('assets/')?>images/recent-profile8.jpg" alt="">
                      </figure>
                      <div class="caption"> <a href="#"><strong>Maya Joshi, From Mumbai</strong></a>
                        <p>Age: 20
                          <br>Profession: Self Employed
                          <br>Religion: Hindu
                          <br>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="thumb-pad2 maxheight">
                  <div class="box_inner">
                    <div class="thumbnail">
                      <figure>
                        <img src="<?php echo base_url('assets/')?>images/recent-profile7.jpg" alt="">
                      </figure>
                      <div class="caption"> <a href="#"><strong>Maya Joshi, From Mumbai</strong></a>
                        <p>Age: 20
                          <br>Profession: Self Employed
                          <br>Religion: Hindu
                          <br>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="thumb-pad2 maxheight">
                  <div class="box_inner">
                    <div class="thumbnail">
                      <figure>
                        <img src="<?php echo base_url('assets/')?>images/recent-profile5.jpg" alt="">
                      </figure>
                      <div class="caption"> <a href="#"><strong>Maya Joshi, From Mumbai</strong></a>
                        <p>Age: 20
                          <br>Profession: Self Employed
                          <br>Religion: Hindu
                          <br>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="thumb-pad2 maxheight">
                  <div class="box_inner">
                    <div class="thumbnail">
                      <figure>
                        <img src="<?php echo base_url('assets/')?>images/recent-profile2.jpg" alt="">
                      </figure>
                      <div class="caption"> <a href="#"><strong>Maya Joshi, From Mumbai</strong></a>
                        <p>Age: 20
                          <br>Profession: Self Employed
                          <br>Religion: Hindu
                          <br>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="thumb-pad2 maxheight">
                  <div class="box_inner">
                    <div class="thumbnail">
                      <figure>
                        <img src="<?php echo base_url('assets/')?>images/recent-profile6.jpg" alt="">
                      </figure>
                      <div class="caption"> <a href="#"><strong>Maya Joshi, From Mumbai</strong></a>
                        <p>Age: 20
                          <br>Profession: Self Employed
                          <br>Religion: Hindu
                          <br>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="thumb-pad2 maxheight">
                  <div class="box_inner">
                    <div class="thumbnail">
                      <figure>
                        <img src="<?php echo base_url('assets/')?>images/recent-profile5.jpg" alt="">
                      </figure>
                      <div class="caption"> <a href="#"><strong>Maya Joshi, From Mumbai</strong></a>
                        <p>Age: 20
                          <br>Profession: Self Employed
                          <br>Religion: Hindu
                          <br>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="thumb-pad2 maxheight">
                  <div class="box_inner">
                    <div class="thumbnail">
                      <figure>
                        <img src="<?php echo base_url('assets/')?>images/recent-profile1.jpg" alt="">
                      </figure>
                      <div class="caption"> <a href="#"><strong>Maya Joshi, From Mumbai</strong></a>
                        <p>Age: 20
                          <br>Profession: Self Employed
                          <br>Religion: Hindu
                          <br>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="btn-center">
                <button type="button" class="btn btn-outline-danger"><img src="<?php echo base_url('assets/')?>images/bride.png">&nbsp;&nbsp;Featured Brides</button>&nbsp;&nbsp;
                <button type="button" class="btn btn-danger"><img src="<?php echo base_url('assets/') ?>images/groom.png">&nbsp;&nbsp;Featured Grooms</button>
              </div>
      </div>
    </section>
    <!-- featured profile end here -->
         <!-- search category section starts here -->
         <section>
            <div class="search-category-section" style="background: url(<?php echo base_url('assets/')?>images/category-banner.jpg);
               background-repeat: no-repeat;background-size: cover;background-attachment: fixed;">
               <div class="container text-center" style="padding: 20px;">
                  <h2 class="title divider mb-3 white-text font-size"><strong>Search Profiles</strong></h2>
                  <div class="col-md-12">
                     <img src="<?php echo base_url('assets/') ?>images/divider.png">
                  </div>
                  <br>
                  <div class="row">
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Loaction</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Religion</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Caste</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Mother Tongue</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Education</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Profession</p>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Martial Status</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Family Value</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Family Type</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Family Status</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Body Type</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Complexion</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Eating Habit</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Smoking Habit</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Drinking Habit</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Height</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Age</p>
                           </a>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class="category-type-section">
                           <a href="" class="text-decoration">
                              <p>Income</p>
                           </a>
                        </div>
                     </div>
                     <div class="search-profile-btn-area">
                        <div class="btn-center">
                           <button type="button" class="btn btn-danger"><img src="<?php echo base_url('assets/') ?>images/advanced-search.png">&nbsp;&nbsp;Advance Search</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <br>
         <!-- search category section ends here -->
         <!-- latest profiles start here -->
    <section>
      <div class="container text-center">
        <h2 class="title divider mb-3"><strong>Explore Latest Profiles</strong></h2>
        <p class="lead">Search from thousands of fresh matrimonial profiles..</p>
        <div class="col-md-12 text-center">
          <img src="<?php echo base_url('assets/')?>images/divider.png">
        </div>
        <br>
        <div class="row mbr-justify-content-center">
          <div class="col-md-4">
            <div class="recent-profile-wrap">
              <div class="ico-wrap">
                <img alt="" src="<?php echo base_url('assets/')?>images/recent-profile1.jpg" width="100px; height: 200px;">
              </div>
              <div class="text-wrap vcenter">
                <h3 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 primary-text font-size-18">Maya From Mumbai</h3>
                <p>Age: 20
                  <br>Profession: Self Employed
                  <br>Religion: Hindu
                  <br>
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="recent-profile-wrap">
              <div class="ico-wrap">
                <img alt="" src="<?php echo base_url('assets/')?>images/recent-profile2.jpg" width="100px;">
              </div>
              <div class="text-wrap vcenter">
                <h3 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 primary-text font-size-18">Maya From Mumbai</h3>
                <p>Age: 20
                  <br>Profession: Self Employed
                  <br>Religion: Hindu
                  <br>
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="recent-profile-wrap">
              <div class="ico-wrap">
                <img alt="" src="<?php echo base_url('assets/')?>images/recent-profile2.jpg" width="100px;">
              </div>
              <div class="text-wrap vcenter">
                <h3 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 primary-text font-size-18">Maya From Mumbai</h3>
                </h2>
                <p>Age: 20
                  <br>Profession: Self Employed
                  <br>Religion: Hindu
                  <br>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row mbr-justify-content-center">
          <div class="col-md-4">
            <div class="recent-profile-wrap">
              <div class="ico-wrap">
                <img alt="" src="<?php echo base_url('assets/')?>images/recent-profile3.jpg" width="100px;">
              </div>
              <div class="text-wrap vcenter">
                <h3 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 primary-text font-size-18">Maya From Mumbai</h3>
                <p>Age: 20
                  <br>Profession: Self Employed
                  <br>Religion: Hindu
                  <br>
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="recent-profile-wrap">
              <div class="ico-wrap">
                <img alt="" src="<?php echo base_url('assets/')?>images/recent-profile5.jpg" width="100px;">
              </div>
              <div class="text-wrap vcenter">
                <h3 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 primary-text font-size-18">Maya From Mumbai</h3>
                <p>Age: 20
                  <br>Profession: Self Employed
                  <br>Religion: Hindu
                  <br>
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="recent-profile-wrap">
              <div class="ico-wrap">
                <img alt="" src="<?php echo base_url('assets/')?>images/recent-profile2.jpg" width="100px;">
              </div>
              <div class="text-wrap vcenter">
                <h3 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 primary-text font-size-18">Maya From Mumbai</h3>
                </h2>
                <p>Age: 20
                  <br>Profession: Self Employed
                  <br>Religion: Hindu
                  <br>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row mbr-justify-content-center">
          <div class="col-md-4">
            <div class="recent-profile-wrap">
              <div class="ico-wrap">
                <img alt="" src="<?php echo base_url('assets/')?>images/recent-profile3.jpg" width="100px;">
              </div>
              <div class="text-wrap vcenter">
                <h3 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 primary-text font-size-18">Maya From Mumbai</h3>
                <p>Age: 20
                  <br>Profession: Self Employed
                  <br>Religion: Hindu
                  <br>
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="recent-profile-wrap">
              <div class="ico-wrap">
                <img alt="" src="<?php echo base_url('assets/')?>images/recent-profile5.jpg" width="100px;">
              </div>
              <div class="text-wrap vcenter">
                <h3 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 primary-text font-size-18">Maya From Mumbai</h3>
                <p>Age: 20
                  <br>Profession: Self Employed
                  <br>Religion: Hindu
                  <br>
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="recent-profile-wrap">
              <div class="ico-wrap">
                <img alt="" src="<?php echo base_url('assets/')?>images/recent-profile2.jpg" width="100px;">
              </div>
              <div class="text-wrap vcenter">
                <h3 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5 primary-text font-size-18">Maya From Mumbai</h3>
                </h2>
                <p>Age: 20
                  <br>Profession: Self Employed
                  <br>Religion: Hindu
                  <br>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
  <div class="btn-center">
                <button type="button" class="btn btn-outline-danger"><img src="<?php echo base_url('assets/')?>images/bride.png">&nbsp;&nbsp;All Brides</button>&nbsp;&nbsp;
                <button type="button" class="btn btn-danger"><img src="<?php echo base_url('assets/')?>images/groom.png">&nbsp;&nbsp;All Grooms</button>
              </div>
  </section>
  <!-- latest profiles ends here -->
        <!-- recent profile section start here -->
  <section>
    <div class="container-fluid">
      <div class="container text-center">
        <h2 class="title divider mb-3"><strong>Sweet Success Stories</strong></h2>
        <p class="lead">Write your matrimonial success story with us...</p>
        <div class="col-md-12 text-center">
          <img src="<?php echo base_url('assets/')?>images/divider.png">
        </div>
        <div class="row blog" style="margin-top: 30px;">
          <div class="col-md-12">
            <div id="blogCarousel" class="carousel slide container-blog" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#blogCarousel" data-slide-to="1"></li>
                <li data-target="#blogCarousel" data-slide-to="2"></li>
              </ol>
              <!-- Carousel items -->
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story1.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story2.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story3.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story4.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--.row-->
                </div>
                <!--.item-->
                <div class="carousel-item ">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story5.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                             <img alt="" src="<?php echo base_url('assets/')?>images/success-story6.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story5.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story2.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--.row-->
                </div>
                <!--.item-->
                <div class="carousel-item">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story1.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story4.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story3.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="item-box-blog">
                        <div class="item-box-blog-image">
                          <!--Image-->
                          <figure>
                            <a href="#">
                              <img alt="" src="<?php echo base_url('assets/')?>images/success-story5.jpg">
                            </a>
                          </figure>
                        </div>
                        <div class="item-box-blog-body">
                          <!--Heading-->
                          <div class="item-box-blog-heading">
                            <a href="#" tabindex="0">
                              <h3 class="font-size-18">Abc weds Xyz</h3>
                            </a>
                          </div>
                          <!--Text-->
                          <!-- <div class="item-box-blog-text">
                            <p>Age: 20
                              <br>Profession: Self Employed
                              <br>Religion: Hindu
                              <br>
                            </p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--.row-->
                </div>
              </div>
              <!--.carousel-inner-->
            </div>
            <!--.Carousel-->
          </div>
        </div>
      </div>
    </div>
  </section>
  <br>
  <!-- recent profile section ends here -->