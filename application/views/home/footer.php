<section>
         <div class="footer">
            <div class="footer-links">
               <div class="container text-center">
                  <div class="row">
                     <div class="col-sm-2">
                        <a href="<?php echo base_url('About-us') ?>" class="text-decoration">About Us</a>
                     </div>
                     <div class="col-sm-2">
                        <a href="<?php echo base_url('Contact-us') ?>" class="text-decoration">Contact Us</a>
                     </div>
                     <div class="col-sm-2">
                        <a href="<?php echo base_url('Disclaimer') ?>" class="text-decoration">Disclaimer</a>
                     </div>
                     <div class="col-sm-2">
                        <a href="<?php echo base_url('Privacy-Policy') ?>" class="text-decoration">Privacy Policy</a>
                     </div>
                     <div class="col-sm-2">
                        <a href="<?php echo base_url('Terms-Of-Service') ?>" class="text-decoration">Terms Of Service</a>
                     </div>
                     <div class="col-sm-2">
                        <a href="" class="text-decoration">Wedding Directory</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="footer-text">
               <p>Copyright © 2019 Navra Bayko  |  <a href="https://www.hiya.digital/">Webiste Designing & Development By Hiya Digital</a></p>
            </div>
         </div>
      </section>
      <!-- footer ends here -->
      </div>
   </body>
</html>