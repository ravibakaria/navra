<section class="blank_section">
  <br/><br/><br/>
</section>
<?php
  if($recaptchaAllowed->recaptchaAllowed == "1")
  {
    echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
  }
?>
<!-- Main Page Content Shown Here  -->
  <div class="row contact-us-page">
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
      <!-- Section Titile -->
      <div class="col-md-12 wow animated fadeInLeft" data-wow-delay=".2s">
          <h1>Contact Us</h1>
          <br>
      </div>

      <div class="">

        <?php
        if(!empty($result))
        {
          $enquiry_email = $result->enquiry_email;
          $companyName = $result->companyName;
          $street = $result->street;
          $city = $result->city;
          $state = $result->state;
          $country = $result->country;
          $postalCode = $result->postalCode;
          $phonecode = $result->phonecode;
          $phone = $result->phone;
          $companyEmail = $result->companyEmail;
          $companyURL = $result->companyURL;

          $city_name = $result->cityname;
          $state_name = $result->statename;
          $country_name = $result->countryname;
        ?>
        <br/>
        <?php
        if($companyName!='' || $companyName!=null)
          {
          ?>
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
              <b>Company Name</b>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
              <?php echo $companyName; ?>
            </div>
          </div>

          <br/>

          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
              <b>Address</b>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
              <?php echo $street ;?>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">

            </div>
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
            <?php echo $city_name.", ".$state_name.", ".$postalCode." ".$country_name; ?>
            </div>
          </div>

          <br/>              

          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
              <b>Phone Number</b>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
              <?php echo "+".$phonecode." - ".$phone; ?>
            </div>
          </div>

          <br/>

          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
              <b>Email Address</b>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-9 text-left">
              <?php echo "<a href='mailto:$companyEmail'>$companyEmail</a>"; ?>
            </div>
          </div>

          <?php
          }
        }
        ?>
        <hr/>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
      <div class="row">
                  <!-- Section Titile -->
                  <div class="col-md-12 wow animated fadeInLeft" data-wow-delay=".2s">
                      <h2>Enquiry Form</h2>
                  </div>
              </div>
              <br/>
                  <form name="frmLogin" id="frmLogin" autocomplete="off" method="post"  action="<?php echo $_SERVER['PHP_SELF'];?>">
                    <div class="form-group">
                    <?php if($this->session->flashdata('message_e')){?>
                        <div class="alert alert-success">
                            <strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
                        </div>
                    <?php } ?> 
                      <?php
                        if($this->session->flashdata('message_e')){
                          echo "<div class='alert alert-danger alert-dismissible order-detail-alert-dismissible col-sm-12 error_status' style='padding: 10px; margin-bottom: 10px;'>";
                          echo "<a href='#' class='close' data-dismiss='alert' aria-label='close' style='top: 0px;right: 0px;'>&times;</a>";
                          echo "<strong>Error!</strong>." .$this->session->flashdata('message_e');
                          echo "</div>";
                        }

                        if($this->session->flashdata('message')){
                          echo "<div class='alert alert-success alert-dismissible order-detail-alert-dismissible col-sm-12' style='padding: 10px; margin-bottom: 10px;'>";
                          echo "<a href='#' class='close' data-dismiss='alert' aria-label='close' style='top: 0px;right: 0px;'>&times;</a>";
                          echo "<strong>Success!</strong> Your enquiry sent successfully.Thank You.";
                          echo "</div>";
                        }
                      ?>
                    </div>

                    <!-- Name -->
                    <div class="form-group label-floating">
                      <label class="control-label">Full Name</label>
                      <input class="form-control fullname" type="text" name="fullname">
                      <div class="fullname-error"></div>
                    </div>
                    <!-- email -->
                    <div class="form-group label-floating">
                      <label class="control-label" for="email">Email Address</label>
                      <input class="form-control email" id="email" type="email" name="email">
                      <div class="email-error"></div>
                    </div>
                    <!-- mobile -->
                    <div class="form-group label-floating">
                      <label class="control-label" for="mobile">Mobile Number</label>
                      <input class="form-control mobile" id="mobile" type="text" name="mobile">
                      <div class="mobile-error"></div>
                    </div>
                    <!-- Subject -->
                    <div class="form-group label-floating">
                      <label class="control-label">Subject</label>
                      <input class="form-control subject" id="subject" type="text" name="subject">
                      <div class="subject-error"></div>
                    </div>
                    <!-- Message -->
                    <div class="form-group label-floating">
                        <label for="message" class="control-label">Message</label>
                        <textarea class="form-control message" rows="3" id="message" name="message"></textarea>
                        <div class="message-error"></div>
                    </div>                    

                    <input type="hidden" class="recaptchaAllowed" value="<?php echo $recaptchaAllowed->recaptchaAllowed;?>">

                    <div class="form-group label-floating centered-data">
                        <center><img src="<?php echo base_url('assets/')?>images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' alt='loader' style='width:60px; height:60px; display:none;'/></center>
                        <div class="contact-us-status"></div>
                        <div class="phonenumber_error"></div>
                    </div>
                    <!-- Form Submit -->
                    <div class="form-submit mt-5">
                        <button type="submit" name="contact_us_btn"  class="btn btn-primary btn-lg btn-block btn-send-message website-button" id="recaptcha-submit"><span class="fa fa-envelope"></span>&nbsp;&nbsp;Send</button>
                        <div class="clearfix"></div>
                    </div>
                    <?php
                      if($recaptchaAllowed->recaptchaAllowed == "1")
                      {
                        echo "<div class='form-group recaptcha-div'>
                          <div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaAllowed->reCaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
                        </div>";
                      }
                    ?>
                    
                    
                </form>
            </div>
    </div>
  </div>
<br/><br/>
</section>

<script>
function onSubmit(token)
    {
        document.getElementById("frmLogin").submit();
    }

    function validate(event) 
    {
      event.preventDefault();
      var fullname = $('.fullname').val();
      var email = $('.email').val();
      var mobile = $('.mobile').val();
      var subject = $('.subject').val();
      var message = $('.message').val();
      var recaptchaAllowed = $('.recaptchaAllowed').val();
      var user = $('.user').val();
      var task = "contact-us-request";
      var empty_success=0;
        
        if(fullname=='' || fullname==null)
        {
          $('.fullname').addClass('danger_error');
          empty_success = empty_success+1;
        }        

        /* Email Validation */
        if(email=='' || email==null)
        {
          $('.email').addClass('danger_error');
          empty_success = empty_success+1;
        }

        function validateEmail($email) {
            var emailReg = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            return emailReg.test( $email );
        }

        if(email!='' && !validateEmail(email)) 
        { 
            $('.email').addClass('danger_error');
            empty_success=empty_success+1;
            $('.contact-us-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong> Invalid! </strong> Email id not valid!. Please enter valid email id.</div>");
            return false;
        }

        if(mobile=='' || mobile==null)
        {
          $('.mobile').addClass('danger_error');
          empty_success = empty_success+1;
        }
        if(subject=='' || subject==null)
        {
          $('.subject').addClass('danger_error');
          empty_success = empty_success+1;
        }
        if(message=='' || message==null)
        {
          $('.message').addClass('danger_error');
          empty_success = empty_success+1;
        }

        if(empty_success>0)
        {
          $('.contact-us-status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong> Empty! </strong> Please enter all required data & then click on send.</div>");
          return false;
        }

        $('.btn-send-message').attr('disabled',true);
      //recaptcha validation 
      if(recaptchaAllowed=='1')
      {
        grecaptcha.execute();
      }
      else
      {
        document.getElementById("frmLogin").submit();
      }

      $('.loading_img').show();
      $('.contact-us-status').html("");
    }

    function onload() 
    {
        var element = document.getElementById('recaptcha-submit');
        element.onclick = validate;
    }
    onload();
  $(document).ready(function(){
    
    /* empty error message  */
    $('.fullname, .email, .mobile, .subject, .message').click(function(){
          $('.contact-us-status').html("");
      });

    /* empty danger class message  */
    $('.fullname').click(function(){
        $('.fullname').removeClass("danger_error");
    });

    $('.email').click(function(){
        $('.email').removeClass("danger_error");
    });

    $('.mobile').click(function(){
        $('.mobile').removeClass("danger_error");
    });

    $('.subject').click(function(){
        $('.subject').removeClass("danger_error");
    });

    $('.message').click(function(){
        $('.message').removeClass("danger_error");
    });

    /*   Prevent entering charaters in mobile & phone number   */
    $(".mobile").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
        {
            //display error message
            $('.phonenumber_error').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Invalid!</strong> Albhabets not allowed. Enter Digits only.</div>").show().fadeOut(3000);
            return false;
        }
    });
  });
</script>