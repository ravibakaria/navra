<div class="container mb-5 mt-5">
    <div class="pricing card-deck flex-column flex-md-row mb-3">
        <div class="card card-pricing popular shadow text-center px-3 mb-4">
           <span class="h1 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm paragraph-text2">BASIC</span>
            <div class="bg-transparent card-header pt-4 border-0">
                <h2 class="h2 font-weight-normal text-primary text-center mb-0 font-size-28" data-pricing-value="45"><span class="price">INR: 10.00</span><!-- <span class="h6 text-muted ml-2">/ per month</span> --></h2>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li>100 Contacts/Chats</li>
                    <li>1 Months Validity</li>
                    <li>Unlimited Profile Views</li>
                    <li>Unlimited Expression Of Interest</li>
                    <li>Unlimited Photo Requests</li>
                    <li>Unlimited Profile Sharing</li>
                </ul>
                <button type="button" class="btn btn-outline-danger">Order Now</button>
            </div>
        </div>
        <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <span class="h1 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm paragraph-text2">SILVER</span>
            <div class="bg-transparent card-header pt-4 border-0">
               <h2 class="h2 font-weight-normal text-primary text-center mb-0 font-size-28" data-pricing-value="45"><span class="price">INR: 199.00</span><!-- <span class="h6 text-muted ml-2">/ per month</span> --></h2>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li>250 Contacts/Chats</li>
                    <li>4 Months Validity</li>
                    <li>Unlimited Profile Views</li>
                    <li>Unlimited Expression Of Interest</li>
                    <li>Unlimited Photo Requests</li>
                    <li>Unlimited Profile Sharing</li>
                </ul>
                <button type="button" class="btn btn-outline-danger">Order Now</button>
            </div>
        </div>
        <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <span class="h1 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm paragraph-text2">GOLD</span>
            <div class="bg-transparent card-header pt-4 border-0">
                <h2 class="h2 font-weight-normal text-primary text-center mb-0 font-size-28" data-pricing-value="45"><span class="price">INR: 499.00</span><!-- <span class="h6 text-muted ml-2">/ per month</span> --></h2>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li>500 Contacts/Chats</li>
                    <li>8 Months Validity</li>
                    <li>Unlimited Profile Views</li>
                    <li>Unlimited Expression Of Interest</li>
                    <li>Unlimited Photo Requests</li>
                    <li>Unlimited Profile Sharing</li>
                </ul>
                <button type="button" class="btn btn-outline-danger">Order Now</button>
            </div>
        </div>
        <div class="card card-pricing popular shadow text-center px-3 mb-4">
            <span class="h1 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm paragraph-text2">PLATINUM</span>
            <div class="bg-transparent card-header pt-4 border-0">
               <h2 class="h2 font-weight-normal text-primary text-center mb-0 font-size-28" data-pricing-value="45"><span class="price">INR: 1499.00</span><!-- <span class="h6 text-muted ml-2">/ per month</span> --></h2>
            </div>
            <div class="card-body pt-0">
                <ul class="list-unstyled mb-4">
                    <li>Unlimited Contacts/Chats</li>
                    <li>12 Months Validity</li>
                    <li>Unlimited Profile Views</li>
                    <li>Unlimited Expression Of Interest</li>
                    <li>Unlimited Photo Requests</li>
                    <li>Unlimited Profile Sharing</li>
                </ul>
                <button type="button" class="btn btn-outline-danger">Order Now</button>
            </div>
        </div>
    </div>
</div>