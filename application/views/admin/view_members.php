<section role="main" class="content-body update-section">

<div class="row admin-start-section">
    <div class="col-md-12 col-lg-12 col-xl-12">
        <h2><?php echo $display_gender.' With '.$display_marital_status.' Status.';?> </h2>
    </div>

    <input type="hidden" class="gender" value="<?php echo $gender;?>" data-column="4">
    <input type="hidden" class="marital_status" value="<?php echo $marital_status;?>" data-column="7">

    <div class="col-md-12 col-lg-12 col-xl-12">
        <table id='datatable-info' class="table-hover table-striped table-bordered datatable view_member_list">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Profile Id</th>
                    <th>Gender</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Marital Status</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
</section>
</div>
</section>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function(){
    var dataTable = $('.view_member_list').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax":{
            url :"<?php echo base_url('admin/view_members_response');?>", // json datasource
           
            type: "post",  // type of method  ,GET/POST/DELETE
            error: function(){
                $(".view_member_list_processing").css("display","none");
            }
        }
    });
});
</script>