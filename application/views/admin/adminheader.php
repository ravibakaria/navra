<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Navra Bayko</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/bootstrap.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">
      <link href="https://fonts.googleapis.com/css?family=Cambo&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/datatables.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/style.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins|Roboto&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lora&display=swap" rel="stylesheet">
      <link rel="icon" type="image/png" href="<?php echo base_url('assets/') ?>images/heart-title.png" sizes="50x50">
      <script src="<?php echo base_url('assets/') ?>/js/jquery.min.js"></script>
      <script src="<?php echo base_url('assets/') ?>js/popper.min.js"></script>
      <script src="<?php echo base_url('assets/') ?>js/bootstrap.min.js"></script>
      <script src="<?php echo base_url('assets/') ?>js/datatables.min.js"></script>
   </head>
   <body>
      <div class="login-wrapper">
         <!-- primary header starts here -->
         <nav class="navbar navbar-expand-lg navbar-light">
            <div class="logo" style="width: 38%">
               <a class="navbar-brand" href="<?php echo base_url() ?>">
               <img src="<?php echo base_url('assets/') ?>images/logo.png" style="height:75px;">
               </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          
               
            </div>
            <ul class="navbar-nav w-100">
                <li class="nav-item dropdown ml-auto">
                <a class="nav-link dropdown-toggle paragraph-text2" href="#" id="navbardrop" data-toggle="dropdown" style="color: white;"> <?php echo  $this->session->userdata('admin')->firstname;?> </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?php echo base_url('admin/change_password')?>"><i class="fa fa-key"></i> Change Password</a>
					<a class="dropdown-item" href="<?php echo base_url('user/adminlogout')?>"><i class="fa fa-power-off"></i> Logout</a>
                        
                    </div>
                </li>
            </ul>
         </nav>
         <!-- primary  header ends here -->
         <div id="wrapper">

<!-- Sidebar -->
<ul class="sidebar navbar-nav">
  <li class="text-center">
    <div class="d-flex justify-content-center h-100">
  <div class="image_outer_container">
    <div class="image_inner_container">
      <img src="<?php echo base_url('assets/')?>images/profile.jpg" style="height:100px; width: 100px;">
    </div>
    <div class="profile-info-text">
      <p class="white-text paragraph-text1">Profile Score 0%</p>
      <p class="white-text">My Profile Status : Active</p>
    </div>
  </div>
</div>
  </li>
  <li class="nav-item active">
    <a class="nav-link" href="<?php echo base_url('user') ?>">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span>
    </a>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="far fa-address-card"></i>
      <span>My Profile</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
      <a class="dropdown-item" href="<?php echo base_url('users/my-profile')?>"><i class='far fa-user'></i>&nbsp;My Profile</a>
      <a class="dropdown-item" href="register.html"><i class='fas fa-portrait'></i>&nbsp;My Photos</a>
      <a class="dropdown-item" href="forgot-password.html"><i class='far fa-copy'></i>&nbsp;My Documents</a>
      <a class="dropdown-item" href="404.html"><i class='far fa-check-square'></i>&nbsp;My Preferences</a>
      <a class="dropdown-item" href="blank.html"><i class='far fa-comments'></i>&nbsp;My Chat Messeges</a>
      <a class="dropdown-item" href="blank.html"><i class="fa fa-lock"></i>&nbsp;Change Password</a>
    </div>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class='fas fa-heart'></i>
      <span>Match Result</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
      <a class="dropdown-item" href="login.html"><i class='fas fa-star'></i>&nbsp;My Matches</a>
      <a class="dropdown-item" href="register.html"><i class='far fa-heart'></i>&nbsp;Shortlisted Profiles</a>
      <a class="dropdown-item" href="forgot-password.html"><i class='far fa-check-circle'></i>&nbsp;Interested Prospects</a>
      <a class="dropdown-item" href="404.html"><i class="fas fa-ban"></i>&nbsp;Blocked Profiles</a>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="tables.html">
      <i class="fas fa-search"></i>
      <span>Advance Search</span></a>
  </li>
      <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class='fas fa-cog'></i>
      <span>Utilities</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
      <a class="dropdown-item" href="login.html"><i class="far fa-thumbs-up"></i>&nbsp;Profile Likes</a>
      <a class="dropdown-item" href="register.html"><i class='fas fa-mail-bulk'></i>&nbsp;Invite Freinds</a>
      <a class="dropdown-item" href="forgot-password.html"><i class='fa fa-window-close'></i>&nbsp;Report Abuse</a>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="<?php echo base_url('users/my-orders')?>">
      <i class='fas fa-boxes'></i>
      <span>My Orders</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="tables.html">
      <i class='far fa-sticky-note'></i>
      <span>Wedding Directory</span></a>
  </li>
</ul>
<div id="content-wrapper">

      <div class="container-fluid"> 