<h3>Total Members</h3>
<div class="row">
  <div class="col-md-12 col-lg-3 col-xl-3">
    <div class="card" style="width: 15rem;">
        <div class="card-header">
        <strong class="amount"><?php if(!empty($getBrideCount)){
                                            echo $getBrideCount->bride_count;
                                            }else{
                                                echo "0";}?>
                                                Brides</strong>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
            <?php 
                if(!empty($getNeverMarrideBrides)==0)
                {
                    echo "Never Married";
                }
                else
                {
                    echo "<strong>".count($getNeverMarrideBrides)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/2/1').'"> 
                        Never Married
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getAwaitingDivorceBrides)==0)
                {
                    echo "Awaiting Divorce";
                }
                else
                {
                    echo "<strong>".count($getAwaitingDivorceBrides)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/2/2').'"> 
                    Awaiting Divorce
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getDivorceBrides)==0)
                {
                    echo "Divorced";
                }
                else
                {
                    echo "<strong>".count($getDivorceBrides)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/2/3').'"> 
                    Divorced
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getWidowedBrides)==0)
                {
                    echo "Widowed";
                }
                else
                {
                    echo "<strong>".count($getWidowedBrides)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/2/4').'"> 
                    Widowed
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getAnnulledBrides)==0)
                {
                    echo "Annulled";
                }
                else
                {
                    echo "<strong>".count($getAnnulledBrides)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/2/5').'"> 
                    Annulled
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getProfileIncompleteBrides)==0)
                {
                    echo " Profile Incomplete";
                }
                else
                {
                    echo "<strong>".count($getProfileIncompleteBrides)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/2/-1').'"> 
                    Profile Incomplete
                    </a>';	
                }
            ?>
            </li>
            
            
        </ul>
        <div class="card-footer text-right">
            (view all)
        </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-3 col-xl-3">
  <div class="card" style="width: 15rem;">
        <div class="card-header">
        <strong class="amount"><?php if(!empty($getGroomCount)){
                                            echo $getGroomCount->bride_count;
                                            }else{
                                                echo "0";}?> Grooms</strong>
        </div>
        <ul class="list-group list-group-flush">
        <li class="list-group-item">
            <?php 
                if(!empty($getNeverMarrideGroom)==0)
                {
                    echo "Never Married";
                }
                else
                {
                    echo "<strong>".count($getNeverMarrideGroom)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/1/1').'"> 
                        Never Married
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getAwaitingDivorceGroom)==0)
                {
                    echo "Awaiting Divorce";
                }
                else
                {
                    echo "<strong>".count($getAwaitingDivorceGroom)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/1/2').'"> 
                    Awaiting Divorce
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getDivorceGroom)==0)
                {
                    echo "Divorced";
                }
                else
                {
                    echo "<strong>".count($getDivorceGroom)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/1/3').'"> 
                    Divorced
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getWidowedGroom)==0)
                {
                    echo "Widowed";
                }
                else
                {
                    echo "<strong>".count($getWidowedGroom)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/1/4').'"> 
                    Widowed
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getAnnulledGroom)==0)
                {
                    echo "Annulled";
                }
                else
                {
                    echo "<strong>".count($getAnnulledGroom)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/1/5').'"> 
                    Annulled
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getProfileIncompleteGroom)==0)
                {
                    echo " Profile Incomplete";
                }
                else
                {
                    echo "<strong>".count($getProfileIncompleteGroom)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/1/-1').'"> 
                    Profile Incomplete
                    </a>';	
                }
            ?>
            </li>
        </ul>
        <div class="card-footer text-right">
            (view all)
        </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-3 col-xl-3">
  <div class="card" style="width: 15rem;">
        <div class="card-header">
        <strong class="amount"><?php if(!empty($getTGenderCount)){
                                            echo $getTGenderCount->bride_count;
                                            }else{
                                                echo "0";}?> Others</strong>
        </div>
        <ul class="list-group list-group-flush">
        <li class="list-group-item">
            <?php 
                if(!empty($getNeverMarrideTGender)==0)
                {
                    echo "Never Married";
                }
                else
                {
                    echo "<strong>".count($getNeverMarrideTGender)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/3/1').'"> 
                        Never Married
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getAwaitingDivorceTGender)==0)
                {
                    echo "Awaiting Divorce";
                }
                else
                {
                    echo "<strong>".count($getAwaitingDivorceTGender)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/3/2').'"> 
                    Awaiting Divorce
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getDivorceTGender)==0)
                {
                    echo "Divorced";
                }
                else
                {
                    echo "<strong>".count($getDivorceTGender)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/3/3').'"> 
                    Divorced
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getWidowedTGender)==0)
                {
                    echo "Widowed";
                }
                else
                {
                    echo "<strong>".count($getWidowedTGender)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/3/4').'"> 
                    Widowed
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getAnnulledTGender)==0)
                {
                    echo "Annulled";
                }
                else
                {
                    echo "<strong>".count($getAnnulledTGender)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/3/5').'"> 
                    Annulled
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if(!empty($getProfileIncompleteTGender)==0)
                {
                    echo " Profile Incomplete";
                }
                else
                {
                    echo "<strong>".count($getProfileIncompleteTGender)."</strong>";
                    echo '<a href="'.base_url('admin/view_members/3/-1').'"> 
                    Profile Incomplete
                    </a>';	
                }
            ?>
            </li>
        </ul>
        <div class="card-footer text-right">
            (view all)
        </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-3 col-xl-3">
  <div class="card" style="width: 15rem;">
        <div class="card-header">

        <strong class="amount"><?php echo $totalCount;?> Total</strong>
        </div>
        <ul class="list-group list-group-flush">
        <li class="list-group-item">
            <?php 
                if($getNeverMarridetotal == 0)
                {
                    echo "Never Married";
                }
                else
                {
                    echo "<strong>".$getNeverMarridetotal."</strong>";
                    echo '<a href="'.base_url('admin/view_members/0/1').'"> 
                        Never Married
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if($getAwaitingDivorcetotal == 0)
                {
                    echo "Awaiting Divorce";
                }
                else
                {
                    echo "<strong>".$getAwaitingDivorcetotal."</strong>";
                    echo '<a href="'.base_url('admin/view_members/0/2').'"> 
                    Awaiting Divorce
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if($getDivorcetotal==0)
                {
                    echo "Divorced";
                }
                else
                {
                    echo "<strong>".$getDivorcetotal."</strong>";
                    echo '<a href="'.base_url('admin/view_members/0/3').'"> 
                    Divorced
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if($getWidowedtotal==0)
                {
                    echo "Widowed";
                }
                else
                {
                    echo "<strong>".$getWidowedtotal."</strong>";
                    echo '<a href="'.base_url('admin/view_members/0/4').'"> 
                    Widowed
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if($getAnnulledtotal==0)
                {
                    echo "Annulled";
                }
                else
                {
                    echo "<strong>".$getAnnulledtotal."</strong>";
                    echo '<a href="'.base_url('admin/view_members/0/5').'"> 
                    Annulled
                    </a>';	
                }
            ?>
            </li>
            <li class="list-group-item">
            <?php 
                if($getProfileIncompletetotal==0)
                {
                    echo " Profile Incomplete";
                }
                else
                {
                    echo "<strong>".$getProfileIncompletetotal."</strong>";
                    echo '<a href="'.base_url('admin/view_members/0/-1').'"> 
                    Profile Incomplete
                    </a>';	
                }
            ?>
            </li>
        </ul>
        <div class="card-footer text-right">
            (view all)
        </div>
    </div>
  </div>
</div>
<h3>Member Status</h3>
<div class="row">
  <div class="col-md-12 col-lg-3 col-xl-3">
  <div class="card" style="width: 15rem;">
        <div class="card-header">
        <strong class="amount"><?php echo $activ; ?> Active</strong>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"></li>
            
        </ul>
        <div class="card-footer text-right">
        (view all)
        </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-3 col-xl-3">
  <div class="card" style="width: 15rem;">
        <div class="card-header">
        <strong class="amount"><?php echo $inactiv; ?> In-Active</strong>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"></li>
            
        </ul>
        <div class="card-footer text-right">
        (view all)
        </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-3 col-xl-3">
  <div class="card" style="width: 15rem;">
        <div class="card-header">
        <strong class="amount"><?php echo $deactiv; ?> Deactivate</strong>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"></li>
           
        </ul>
        <div class="card-footer text-right">
            (view all)
        </div>
    </div>
  </div>
  <div class="col-md-12 col-lg-3 col-xl-3">
  <div class="card" style="width: 15rem;">
        <div class="card-header">
        <strong class="amount"> <?php echo $sespend; ?> Suspend</strong>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"></li>
            
        </ul>
        <div class="card-footer text-right ">
            (view all)
        </div>
    </div>
  </div>
</div>