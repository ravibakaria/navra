<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Navra Bayko</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/bootstrap.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">
      <link href="https://fonts.googleapis.com/css?family=Cambo&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/datatables.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/style.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins|Roboto&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lora&display=swap" rel="stylesheet">
      <link rel="icon" type="image/png" href="<?php echo base_url('assets/') ?>images/heart-title.png" sizes="50x50">
      <script src="<?php echo base_url('assets/') ?>/js/jquery.min.js"></script>
      <script src="<?php echo base_url('assets/') ?>js/popper.min.js"></script>
      <script src="<?php echo base_url('assets/') ?>js/bootstrap.min.js"></script>
      <script src="<?php echo base_url('assets/') ?>js/datatables.min.js"></script>
   </head>
   <body>
      <div class="login-wrapper">
         <!-- primary header starts here -->
         <nav class="navbar navbar-expand-lg navbar-light">
            <div class="logo" style="width: 38%">
               <a class="navbar-brand" href="<?php echo base_url() ?>">
               <img src="<?php echo base_url('assets/') ?>images/logo.png" style="height:75px;">
               </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span>
            </button>
           
           
         </nav>
         <!-- primary  header ends here -->

<?php
	if($recaptchaAllowed->recaptchaAllowed == "1")
	{
		echo "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
	}
?>
<section class="login-block" style="background-image: url(<?php echo base_url('assets/')?>images/banner-slider1.jpg) !important;
    background-size: cover;
    background-repeat: no-repeat;">
  <div id="login">
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form name="frmLogin" id="frmLogin" class="form" action="<?php echo base_url('login/adminlogin_action')?>" method="post">
                            <h1 class="text-center primary-text font-size-30">Login</h1><br>
                            <?php if($this->session->flashdata('message')){?>
                                <div class="alert alert-success">
                                    <strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
                                </div>
                                <?php } ?>
                                <?php if($this->session->flashdata('message_e')){?>
                                <div class="alert alert-warning">
                                    <strong>Error!</strong> <?php echo $this->session->flashdata('message_e');?>.
                                </div>
                                <?php } ?>
                            <div class="form-group">
                                <label for="username" class="primary-text paragraph-text2"> User Name:</label><br>
                                <input type="email" name="email" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="primary-text paragraph-text2">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                            <br>
                            <input type="hidden" class="recaptchaAllowed" name="recaptchaAllowed" value="<?php echo $recaptchaAllowed->recaptchaAllowed;?>">
						
                            
                            <div class="mb-xs text-center server_data_status">
                            
						    </div>
                            <div class=" mb-xs text-center">
                                <img src="<?php echo base_url('assets/')?>images/loader/loader.gif" class='img-responsive loading_img centered-loading-image' id='loading_img' alt='loading' style='width:80px; height:80px; display:none;'/>
                            </div>
                            <div class="form-group">
                                
                                 <button class="btn btn-outline-danger" id="recaptcha-submit"><i class="fas fa-sign-in-alt" style="font-size:14px"></i> Login</button><br>

                                <div class="text-center">
                                 <a href="<?php echo base_url('login/forgotpassword')?>" class="secondary-text paragraph-text2">Forgot Password ?</a>
                              </div>
                                
                            <?php
							if($recaptchaAllowed->recaptchaAllowed == "1")
							{
								echo "<div class='form-group'>
									<div id='recaptcha' class='g-recaptcha' data-sitekey='$recaptchaAllowed->reCaptchaSiteKey' data-callback='onSubmit' data-size='invisible' data-badge='bottomright' align='center'></div>
								</div>";
							}
						?>
						
						<script></script>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
		function onSubmit(token)
		{
		    document.getElementById("frmLogin").submit();
		}

		function validate(event) 
		{
		    event.preventDefault();
		    var email = $('#email').val();
			var password = $('#password').val();
			var recaptchaAllowed = $('.recaptchaAllowed').val();
			var task = "login_client_user";

			/* Email Validation */
			if(email=='' || email==null)
	        {
	        	$('.server_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty! </strong> Enter email address.</div>");
	            return false;
	        }

	        /* Password validation */
	        if(password=='' || password==null)
	        {
	        	$('.server_data_status').html("<div class='alert alert-danger' style='padding: 5px; margin-bottom: 10px;margin-top: 2px;'><strong>Empty! </strong> Enter your password.</div>");
	            return false;
	        }

	        //recaptcha validation 
	        if(recaptchaAllowed=='1')
			{
				grecaptcha.execute();
			}
			else
			{
				document.getElementById("frmLogin").submit();
			}

			$('.loading_img').show();
			$('.server_data_status').html("");
		}

		function onload() 
		{
		    var element = document.getElementById('recaptcha-submit');
		    element.onclick = validate;
		}
        onload();
	</script>
    <section>
         <div class="footer">
            <div class="footer-links">
               <div class="container text-center">
                  <div class="row">
                     <div class="col-sm-2">
                        <a href="<?php echo base_url('About-us') ?>" class="text-decoration">About Us</a>
                     </div>
                     <div class="col-sm-2">
                        <a href="<?php echo base_url('Contact-us') ?>" class="text-decoration">Contact Us</a>
                     </div>
                     <div class="col-sm-2">
                        <a href="<?php echo base_url('Disclaimer') ?>" class="text-decoration">Disclaimer</a>
                     </div>
                     <div class="col-sm-2">
                        <a href="<?php echo base_url('Privacy-Policy') ?>" class="text-decoration">Privacy Policy</a>
                     </div>
                     <div class="col-sm-2">
                        <a href="<?php echo base_url('Terms-Of-Service') ?>" class="text-decoration">Terms Of Service</a>
                     </div>
                     <div class="col-sm-2">
                        <a href="" class="text-decoration">Wedding Directory</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="footer-text">
               <p>Copyright © 2019 Navra Bayko  |  <a href="https://www.hiya.digital/">Webiste Designing & Development By Hiya Digital</a></p>
            </div>
         </div>
      </section>
      <!-- footer ends here -->
      </div>
   </body>
</html>