<?php 
$user_id =$this->session->userdata('user')->id;
?>
		
			<div class='row'>
				<div class='advanced-search-result-header'>
					<h1>Interested Prospects.</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
						<div class="form-group">
							<input class="form-check-input interest_profile" type="radio" name="interest_profile" id="interest_profile1" value="Interest_By_Me" <?php if($interest==null || $interest=='sent') { echo 'checked';} ?>>

							<label for="interest_profile1" class="control-label profile_info_data">Interest Sent</label>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="form-group">
							<input class="form-check-input interest_profile" type="radio" name="interest_profile" id="interest_profile2" value="Interest_By_Others" <?php if($interest=='received') { echo 'checked';} ?>>
							<label for="interest_profile2" class="control-label profile_info_data">Interest Received</label>
						</div>
					</div>
					<input type="hidden" class="user_id_x" value="<?php echo $user_id;?>" />
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
					<div class="row">
						<div class="result_data interested-prodpects-result">
							
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		          	
				</div>
			</div>
			
			
		<!-- end: page -->

		

<script>
	$(document).ready(function(){
		var value = $('input[type=radio][name=interest_profile]').val();
		var user_id_x = $('.user_id_x').val();
		var task = "Get_Interested_Profile";
		
		var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

		$.ajax({
			type:'post',
        	data:data,
        	url:'<?php echo base_url('user/interest_profiles_helper');?>',
        	success:function(res)
        	{
        		$('.result_data').html(res);
        	}
        });

        $('input[type=radio][name=interest_profile]').change(function() {
			var value = $(this).val();
			var user_id_x = $('.user_id_x').val();
			var task = "Get_Interested_Profile";
			
			var data = 'value='+value+'&user_id_x='+user_id_x+'&task='+task;

			$.ajax({
				type:'post',
            	data:data,
            	url:'<?php echo base_url('user/interest_profiles_helper');?>',
            	success:function(res)
            	{
            		$('.result_data').html(res);
            	}
            });
		});
    });
</script>