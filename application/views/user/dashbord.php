


        <!-- Icon Cards-->
        <div class="row">
          <div class="col-xl-4 col-sm-6 mb-4">
            <div class="card text-white bg-primary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-views"></i>
                </div>
                <div class="card-heading-text paragraph-text1"><?php echo count($ProfileViewCount)?>  Profile Views</div>
              </div>
              <a class="card-footer white-text clearfix small z-1" href="#">
                <span class="float-left card-footer-link">More Info</span>
                <span class="float-right">
                  <i class="fas fa-angle-right icon-next"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-4 col-sm-6 mb-4">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-plan"></i>
                </div>
                <div class="card-heading-text paragraph-text1"><?php echo count($membership)?> Membership Plan Details</div>
              </div>
              <a class="card-footer white-text clearfix small z-1" href="<?php echo base_url('users/my-orders')?>">
                <span class="float-left card-footer-link">More Info</span>
                <span class="float-right">
                  <i class="fas fa-angle-right icon-next"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-4 col-sm-6 mb-4">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-star-o"></i>
                </div>
                <div class="card-heading-text paragraph-text1"><?php 
                  echo count($getMemberIsFeaturedOrNot);                  
                ?> Featured Listing</div>
              </div>
              <a class="card-footer white-text clearfix small z-1" href="<?php echo base_url('users/my-orders')?>">
                <span class="float-left card-footer-link">More Info</span>
                <span class="float-right">
                  <i class="fas fa-angle-right icon-next"></i>
                </span>
              </a>
            </div>
          </div>

        </div>

        <br>

        <div class="row">
          <div class="col-xl-4 col-sm-6 mb-4">
            <div class="card text-white bg-primary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-heart"></i>
                </div>
                <div class="card-heading-text paragraph-text1"><?php echo count($getUserInterestReceived);?> Interest Received</div>
              </div>
              <a class="card-footer white-text clearfix small z-1" href="<?php echo base_url('users/interested-prospects?interest=received')?>">
                <span class="float-left card-footer-link">More Info</span>
                <span class="float-right">
                  <i class="fas fa-angle-right icon-next"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-4 col-sm-6 mb-4">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-check-square-o"></i>
                </div>
                <div class="card-heading-text paragraph-text1"><?php echo count($getUsershortlistedProfiles);?> Shortlisted Profiles</div>
              </div>
              <a class="card-footer white-text clearfix small z-1" href="<?php echo base_url('users/shortlisted-profiles?shortlist=by_me')?>">
                <span class="float-left card-footer-link">More Info</span>
                <span class="float-right">
                  <i class="fas fa-angle-right icon-next"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-4 col-sm-6 mb-4">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-check"></i>
                </div>
                <div class="card-heading-text paragraph-text1"><?php echo count($getUsershortlistedYourProfile);?> Users Shortlisted Your Profile</div>
              </div>
              <a class="card-footer white-text clearfix small z-1" href="<?php echo base_url('users/shortlisted-profiles?shortlist=by_others')?>">
                <span class="float-left card-footer-link">More Info</span>
                <span class="float-right">
                  <i class="fas fa-angle-right icon-next"></i>
                </span>
              </a>
            </div>
          </div>
        </div><br>
        <div class="row">
          <div class="col-xl-4 col-sm-6 mb-4">
            <div class="card text-white bg-primary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-star-o"></i>
                </div>
                <div class="card-heading-text paragraph-text1"><?php echo $getUserMatchCount ?> My Matches</div>
              </div>
              <a class="card-footer white-text clearfix small z-1" href="<?php echo base_url('users/my-matches')?>">
                <span class="float-left card-footer-link">More Info</span>
                <span class="float-right">
                  <i class="fas fa-angle-right icon-next"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-4 col-sm-6 mb-4">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-comments-o"></i>
                </div>
                <div class="card-heading-text paragraph-text1"><?php echo $getUserNewMessage;?> New Messages Received</div>
              </div>
              <a class="card-footer white-text clearfix small z-1" href="#">
                <span class="float-left card-footer-link">More Info</span>
                <span class="float-right">
                  <i class="fas fa-angle-right icon-next"></i>
                </span>
              </a>
            </div>
          </div>
                    <div class="col-xl-4 col-sm-6 mb-4">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-interest"></i>
                </div>
                <div class="card-heading-text paragraph-text1"><?php echo $getUserInterestSent;?>  Interest Sent</div>
              </div>
              <a class="card-footer white-text clearfix small z-1" href="<?php echo base_url('users/interested-prospects?interest=sent')?>">
                <span class="float-left card-footer-link">More Info</span>
                <span class="float-right">
                  <i class="fas fa-angle-right icon-next"></i>
                </span>
              </a>
            </div>
          </div>
        </div>



      

