<?php
	

	
    $logo =  "<img src='".base_url('/assets/images/logo.png')."' class='img-responsive payment-receipt-logo-img' style='padding-left:35px;padding-bottom:5px;'/>";
   
   	if(empty($result_get_pp_details))
   	{
        //    echo "<pre>";
        //    print_r($result_get_pp_details);die;
   		echo  "<center><h1 style='color:red;'>ERROR! Invalid Parameter.<h1></center><br>";
   		
   	}else{

//    echo "<pre>";
//    print_r($result_get_pp_details);die;
	$receipt_number = $result_get_pp_details->id;
	$userid = $result_get_pp_details->userid;
	$OrderNumber = $result_get_pp_details->OrderNumber;
	$membership_plan = $result_get_pp_details->membership_plan;
	$membership_plan_name = $result_get_pp_details->membership_plan_name;
	$membership_plan_id = $result_get_pp_details->membership_plan_id;
	$membership_contacts = $result_get_pp_details->membership_contacts;
	$membership_plan_amount = $result_get_pp_details->membership_plan_amount;
	$membership_plan_expiry_date = $result_get_pp_details->membership_plan_expiry_date;
	$featured_listing = $result_get_pp_details->featured_listing;
	$featured_listing_amount = $result_get_pp_details->featured_listing_amount;
	$featured_listing_expiry_date = $result_get_pp_details->featured_listing_expiry_date;
	$tax_applied = $result_get_pp_details->tax_applied;
	$tax_name = $result_get_pp_details->tax_name;
	$tax_percent = number_format(round($result_get_pp_details->tax_percent,2),2,'.','');
	$tax_amount = $result_get_pp_details->tax_amount;
	$total_amount = $result_get_pp_details->total_amount;
	$tenure = $result_get_pp_details->tenure;
	$transact_id = $result_get_pp_details->transact_id;
	$request_id = $result_get_pp_details->request_id;
	$payment_gateway = $result_get_pp_details->payment_gateway;
	$payment_method = $result_get_pp_details->payment_method;
	$created_at = $result_get_pp_details->created_at;
	$status = $result_get_pp_details->status;

	if($status=='0')
	{
		$status_display = 'UNPAID';
	}
	else
	if($status=='1')
	{
		$status_display = 'PAID';
	}
	else
	if($status=='2')
	{
		$status_display = 'CANCELED';
	}
	else
	if($status=='3')
	{
		$status_display = 'REFUND';
	}


	if($membership_contacts=='0')
	{
		$membership_contacts="Unlimited";
	}

	/************   User info Details start   **************/
	$first_name = $this->session->userdata('user')->firstname;
	$last_name = $this->session->userdata('user')->lastname;
	$user_name = $first_name." ".$last_name;
	$email = $this->session->userdata('user')->email;
	$mobile = $this->session->userdata('user')->phonenumber;
	$phone = '+'.$PhoneCode.'-'.$mobile;
	$profileId = $this->session->userdata('user')->unique_code;

	/************   User info Details End   **************/

	// $DefaultCurrency = getDefaultCurrency();
    // $DefaultCurrencyCode = getDefaultCurrencyCode($DefaultCurrency);

    $purchase_date = date('d F Y',strtotime($created_at));

    $subtotal = number_format(round($membership_plan_amount+$featured_listing_amount,2),2,'.','');

    $total_tax = number_format(round($tax_amount,2),2,'.','');
    $total_amount_paid = number_format(round($total_amount,2),2,'.','');	

	echo  "<div class='container'>
		<div class='row'>
	        <div class='well payment-summary-well col-xs-12 col-sm-12 col-md-7 col-md-offset-3'>
	        	<div class='row payment-logo-row' style='width:110%;margin-left:-19px;'>
					<div class='col-xs-12 col-sm-12 col-md-12'>
						$logo
					</div>
				</div>
				<br/>
				<div class='row'>
					<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center'>
						<h3>Payment Receipt</h3>
						<br>
					</div>
				</div>

	            <div class='row'>
	                <div class='col-xs-6 col-sm-6 col-md-6'>
	                	<strong>To:</strong>
	                    <address>
	                        <strong>$user_name</strong>
	                        <br>
	                        Email: $email
	                        <br>
	                        Profile Id: $profileId
	                        <br>
	                        Phone: $phone
	                    </address>
	                </div>
	                <div class='col-xs-6 col-sm-6 col-md-6 text-right'>
	                	<br>
	                	<address>
	                        <strong>Date:</strong>
	                        $purchase_date<br>
	                        <strong>Receipt Number:</strong>
	                        $receipt_number<br>
	                        <strong>Order Number:</strong>
	                        $OrderNumber<br>
	                        <strong>Payment Status:</strong>
	                        $status_display
	                    </address>
	                </div>
	            </div>

	            <div class='row'>
	            	<div class='col-xs-12 col-sm-12 col-md-12'>
		                <table id='datatable-info' class='table-responsive table-hover table-bordered' style='width:100%'>
		                    <thead>
		                        <tr>
		                            <th>Description</th>
		                            <th>Amount</th>
		                        </tr>
		                    </thead>
		                    <tbody>";
	?>
		                    	<?php
		                    		if($membership_plan=='Yes')
		                    		{
		                    			$membership_plan_description = $membership_plan_name." Membership Plan - ".$membership_contacts." Contacts - Validity ".$tenure." Months";
		                    			echo "<tr>";
		                    				echo "<td>".$membership_plan_description."</td>";
		                    				echo "<td style='text-align:right'>".$DefaultCurrencyCode.":".$membership_plan_amount."</td>";
		                    			echo "</tr>";
		                    		}
		                    	?>
		                     
		                        <?php
		                        	if($featured_listing=='Yes')
		                        	{
		                        		$featured_plan_description = " Featured Listing -  Validity ".$tenure." Months";
		                    			echo "<tr>";
		                    				echo "<td>".$featured_plan_description."</td>";
		                    				echo "<td style='text-align:right'>".$DefaultCurrencyCode.":".$featured_listing_amount."</td>";
		                    			echo "</tr>";
		                    		}
		                        

		                        	echo "<tr>
			                        	<td style='text-align:right'><b>Sub Total<b></td>
			                        	<td style='text-align:right'> $DefaultCurrencyCode:$subtotal</td>
			                        </tr>";
								?>
		                        <?php
		                        	if($tax_applied=='1')
		                        	{
		                        		echo "<tr>";
		                    				echo "<td style='text-align:right'><b>".$tax_percent."% ".$tax_name."</b></td>";
		                    				echo "<td style='text-align:right'>".$DefaultCurrencyCode.":".$tax_amount."</td>";
		                    			echo "</tr>";
		                        	}
		                        ?>

		                        <?php
		                        	echo "<tr>";
	                    				echo "<td style='text-align:right'><b>Total</b></td>";
	                    				echo "<td style='text-align:right'>".$DefaultCurrencyCode.":".$total_amount."</td>";
	                    			echo "</tr>";
		                        
		                    echo "</tbody>
							                </table>
							            </div>
						            </div>

						            <br/>";

	            echo "<div class='row'>
	            	<div class='col-xs-12 col-sm-12 col-md-12'>
	                	<table id='datatable-info' class='table-responsive table-hover table-bordered' style='width:100%''>
	                		<tr>
	                            <th>Transaction Date</th>
	                            <td>$purchase_date</td>
	                        </tr>
	                        <tr>
	                            <th>Gateway</th>
	                            <td>$payment_gateway</td>
	                        </tr>
	                        <tr>
	                            <th>Payment Method</th>
	                            <td>$payment_method</td>
	                        </tr>
	                        <tr>
	                            <th>Transaction ID</th>
	                            <td>$transact_id</td>
	                        </tr>
	                        <tr>
	                            <th>Amount</th>
	                            <td>$DefaultCurrencyCode:$total_amount</td>
	                        </tr>
	                	</table>
	                </div>
	            </div>
	            <br/><br/>
	            <div class='row payment-footer-row'>
					<div class='col-xs-12 col-sm-12 col-md-12'>
						<br/>
						<h4></h4>";
	?>
						<?php
							
                                if(!empty($result))
                                {
                                  $enquiry_email = $result->enquiry_email;
                                  $companyName = $result->companyName;
                                  $street = $result->street;
                                  $city = $result->city;
                                  $state = $result->state;
                                  $country = $result->country;
                                  $postalCode = $result->postalCode;
                                  $phonecode = $result->phonecode;
                                  $phone = $result->phone;
                                  $companyEmail = $result->companyEmail;
                                  $companyURL = $result->companyURL;
                        
                                  $city_name = $result->cityname;
                                  $state_name = $result->statename;
                                  $country_name = $result->countryname;
                                

		                        echo "<b>Address: ".$street.", ".$city_name." - ".$postalCode.", ".$state_name." ".$country_name."</b><br/>";
		                        echo "<b>Phone: +".$phonecode." ".$phone." | Email: ".$companyEmail."</b><br/>";
		                        echo "<b>Website: ".$companyURL."</b>";
		                    }

						echo "<br/>
					</div>
				</div>
	        </div>
	    </div>
    </div>";
}
?>