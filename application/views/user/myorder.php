<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
            <th>Receipt#</th>
                <th style="width:70px;">Activation<br/> Date</th>
                <th>Order#</th>
                <th>Plan Name</th>
                <th>Status</th>
                <th>Expiry Date</th>
                <th>Status</th>
                <th>Expiry Date</th>
                <th>Tenure</th>
                <th>Total Price</th>
                <th>Transaction Status</th>
                <th>Payment Gateway</th>
                <th><center>View</center></th>
                <th><center>Print</center></th>
            </tr>
        </thead>
        <tbody>
        <?php
        if(count($result_order_list)>0){
            foreach ($result_order_list as $row_order_list) {
                $id = $row_order_list['id'];
                $OrderNumber = $row_order_list['OrderNumber'];
                $membership_plan = $row_order_list['membership_plan'];
                $membership_plan_name = $row_order_list['membership_plan_name'];
                $membership_contacts = $row_order_list['membership_contacts'];
                $membership_plan_amount = $row_order_list['membership_plan_amount'];
                $membership_plan_expiry_date = $row_order_list['membership_plan_expiry_date'];
                $featured_listing = $row_order_list['featured_listing'];
                $featured_listing_amount = $row_order_list['featured_listing_amount'];
                $featured_listing_expiry_date = $row_order_list['featured_listing_expiry_date'];
                $tax_applied = $row_order_list['tax_applied'];
                $tax_name = $row_order_list['tax_name'];
                $tax_percent = $row_order_list['tax_percent'];
                $tax_amount = $row_order_list['tax_amount'];
                $total_amount = $row_order_list['total_amount'];
                $tenure = $row_order_list['tenure'];
                $transact_id = $row_order_list['transact_id'];
                $request_id = $row_order_list['request_id'];
                $payment_gateway = $row_order_list['payment_gateway'];
                $payment_method = $row_order_list['payment_method'];
                $created_at = $row_order_list['created_at'];
                $status = $row_order_list['status'];

                if($status=='0')
                {
                    $status_display = "<center><b class='label label-warning'>Unpaid</b></center>";
                }
                else
                if($status=='1')
                {
                    $status_display = "<center><b class='label label-success'>Paid</b></center>";
                }
                else
                if($status=='2')
                {
                    $status_display = "<center><b class='label label-danger'>Canceled</b></center>";
                }
                else
                if($status=='3')
                {
                    $status_display = "<center><b class='label label-info'>Refund</b></center>";
                }

                echo "<tr>";

                if($status=='1')
                {
                    echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".$id."</a></td>";
                    echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".date('d-F-Y',strtotime($created_at))."</a></td>";
                    echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".$OrderNumber."</a></td>";	
                }	
                else
                {
                    echo "<td>".$id."</td>";
                    echo "<td>".date('d-F-Y',strtotime($created_at))."</td>";
                    echo "<td>".$OrderNumber."</td>";	
                }


                    
                if($membership_plan=='Yes' && $status=='1')
                {
                    echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".$membership_plan_name."</a></td>";
                    echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:green'><center><strong><i class='fa fa-check'></strong></center></a></td>";
                    if($membership_plan_expiry_date<$today)
                    {
                        echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:red'>".date('d-M-Y',strtotime($membership_plan_expiry_date))."</a></td>";
                    }
                    else
                    if($membership_plan_expiry_date>=$today)
                    {
                        echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000;'>".date('d-M-Y',strtotime($membership_plan_expiry_date))."</a></td>";
                    }
                    
                }
                else
                {
                    echo "<td>".$membership_plan_name."</td>";
                    echo "<td><center><strong>-</strong></center></td>";
                    echo "<td><center><strong>-</strong></center></td>";
                }

                if($featured_listing=='Yes' && $status=='1')
                {
                    echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:green'><center><strong><i class='fa fa-check'></strong></center></a></td>";
                    if($featured_listing_expiry_date<$today)
                    {
                        echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:red;'>".date('d-M-Y',strtotime($featured_listing_expiry_date))."</a></td>";
                    }
                    else
                    if($featured_listing_expiry_date>=$today)
                    {
                        echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".date('d-M-Y',strtotime($featured_listing_expiry_date))."</a></td>";
                    }
                }
                else
                {
                    echo "<td><center><strong>-</strong></center></td>";
                    echo "<td><center><strong>-</strong></center></td>";
                }

                if($status=='1')
                {
                    echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".$tenure." Months</a></td>";
                    echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;color:#000000'>".$DefaultCurrencyCode.":".$total_amount."</a></td>";
                    echo "<td><a href='' data-toggle='modal' data-target='#plan_".$id."' style='text-decoration:none;'>".$status_display."</a></td>";
                    echo "<td><center><strong>".$payment_gateway."</strong></center></td>";
                    echo "<td><button class='btn btn-sm' data-toggle='modal' data-target='#plan_".$id."' style='background-color:Transparent;'><img src='".base_url('assets')."/images/view-icon.png' style='height:25px;width:25px;'></button></td>";
                    echo "<td><button class='btn btn-sm btn_print_receipt ' id=".$id."  style='background-color:Transparent;' ><img src='".base_url('assets')."/images/print-icon.png' style='height:25px;width:25px;'> </button></td>";
                }
                else
                {
                    echo "<td>".$tenure." Months</td>";
                    echo "<td>".$DefaultCurrencyCode.":".$total_amount."</td>";
                    echo "<td>".$status_display."</td>";
                    echo "<td><center><strong>".$payment_gateway."</strong></center></td>";
                    echo "<td><center><b>-</b></center></td>";
                    echo "<td><button class='btn btn-sm btn_print_receipt ' id=".$id."  style='background-color:Transparent;' ><img src='".base_url('assets')."/images/print-icon.png' style='height:25px;width:25px;'> </button></td>";
                }

                    
                echo "</tr>";
            }
        }

        ?>
        </tbody>
        <tfoot>
            <tr>
            <th>Receipt#</th>
                <th style="width:70px;">Activation<br/> Date</th>
                <th>Order#</th>
                <th>Plan Name</th>
                <th>Status</th>
                <th>Expiry Date</th>
                <th>Status</th>
                <th>Expiry Date</th>
                <th>Tenure</th>
                <th>Total Price</th>
                <th>Transaction Status</th>
                <th>Payment Gateway</th>
                <th><center>View</center></th>
                <th><center>Print</center></th>
            </tr>
        </tfoot>
    </table>
    <div>
        <center><img src="../images/loader/loader.gif" class='img-responsive loading_img' id='loading_img' style='width:80px; height:80px; display:none;'/></center>
    </div>

    <div class="receipt-print" id="receipt_print" style="display:none;">
        <!--     receipt data     -->
    </div>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );


function printContent(el)
		{
			var restorepage = $('body').html();
			var printcontent = $('#' + el).clone();
			$('body').empty().html(printcontent);
			window.print();
			$('body').html(restorepage);
		}

		$('.btn_print_receipt').click(function(){
			var receipt_id = $(this).attr('id');
			var task = "fetch_receipt_print";

			$('.loading_img').show();
			var data = 'receipt_id='+receipt_id+'&task='+task;

			$.ajax({
                type:'post',
                data:data,
                url:"<?php echo base_url('user/order_receipt')?>",
                success:function(res)
                {
                    $('.loading_img').hide();
                    $('.receipt-print').html(res);
                    $('.receipt-print').show();
                    printContent('receipt_print');
                    $('.receipt-print').html("");
                   // location.reload();
                }
            });
		});

</script>