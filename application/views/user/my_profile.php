<section>
            <div class="container">
               <div class="row">
                  <div class="col-md-4 col-xs-12 col-sm-6 col-lg-4">
                     <div class="profile-photo" data-ride="carousel">
                        <img src="images/profile3.jpg">
                        <h3 class="paragraph-text2">Profile Score</h3>
                              <div class="progress">
                                 <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                              </div>
                     </div>
                  </div>
                  <div class="col-md-8 col-xs-12 col-sm-6 col-lg-8">
                     <div class="container">
                        <div class="row">
                           <div class="col-md-9">
                              <h2 class="primary-text font-size-22">Abhijit GXX Your (Profile ID: FKBOH14828)</h2>
                              
                           </div>
                           <div class="col-md-3">
                              <a href="my-profile.php?action2=religious"><button type="button" class="btn btn-outline-danger btn-sm"><i class="fa fa-edit" style="font-size:16px"></i>&nbsp;&nbsp;Edit Profile</button></a>  
                           </div>
                        </div>
                     </div>
                     <br>
                     <ul class="container details">
                        <div class="col-md-12">
                           <div class="row">
                              <div class="col-md-5">
                                 <li>
                                   <a href="#" class="badge badge-success"><i class="fa fa-check"  style="font-size:12px"></i>&nbsp; Profile Picture Updated</a>
                                 </li>
                                 <li>
                                   <a href="#" class="badge badge-success"><i class="fa fa-check"  style="font-size:12px"></i>&nbsp; Profile Picture Updated</a>
                                 </li>
                                 <li>
                                   <a href="#" class="badge badge-success"><i class="fa fa-check"  style="font-size:12px"></i>&nbsp; Profile Picture Updated</a>
                                 </li><br>
                                 <li><a href="#">
                           <button type="button" class="btn btn-danger"><i class="fas fa-ban" style="font-size:14px"></i>&nbsp;&nbsp;Deactivate Profile</button>
                           </a>&nbsp;</li>
                              </div>
                              <div class="col-md-5">
                                 <li>
                                    <a href="#" class="badge badge-danger"><i class="fa fa-window-close" style="font-size:12px"></i>&nbsp; Update Family Info</a>
                                 </li>
                                 <li>
                                   <a href="#" class="badge badge-danger"><i class="fa fa-window-close" style="font-size:12px"></i>&nbsp; Update Family Info</a>
                                 </li>
                                 <li>
                                    <a href="#" class="badge badge-danger"><i class="fa fa-window-close" style="font-size:12px"></i>&nbsp; Update About Yourself</a>
                                 </li>

                              </div>
                           </div>
                        </div>
                           
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div id="nri_profile_b_wrapper">
                        <div>
                           <div class="about_section_ttl">
                              <h2 class="font-size-24">Basic Information</h2>
                           </div>
                           <div id="about_self_nri" class="detail_section pos_rel">
                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 profile_info_data">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>First Name  :</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                Abhijit GXXXXXXXXX                                       
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p class="profile_search_description">Last Name :</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                xyzz                                  
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 profile_info_data">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Profile ID  :</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                FKBOH14828                                     
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p class="profile_search_description">Birth Date :</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                09-Apr-1990                                     
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Height (cm/mtr/foot):</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                162-cms / 5.31-fts / 1.62-mts                                     
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Body Type:</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                Athletic                                   
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Weight (kg):</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                72                                     
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Complexion:</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                Light Or Pale White                                   
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Eating Habit:</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                Vegetarian                                 
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Marital Status:</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                Never Married                              
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Smoking Habit:</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                No Never                             
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Special Case:</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                None of the above                                     
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Drinking Habit:</p>
                                          </div>
                                          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                No Never                                
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                       <div class="row">
                                          <!--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                             <p>Date Of Birth:</p>
                                             </div>
                                             <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                             <p class="profile_search_description">
                                                1990-04-09                                      </p>
                                             </div>-->
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="lifestyle_section_ttl">
                           <h2 class="font-size-24">Religious Information</h2>
                        </div>
                        <div id="about_self_nri" class="detail_section pos_rel">
                           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <div class="row">
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Religion :</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                            Hindu                              
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Caste :</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                            Maratha                             
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Mother Tongue :</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                             Marathi                            
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </div>

                        <div class="lifestyle_section_ttl">
                           <h2 class="font-size-24">Education / Occupation Information</h2>
                        </div>
                        <div id="about_self_nri" class="detail_section pos_rel">
                           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <div class="row">
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Education:</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                             B.E                                
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Employment/ Occupation:</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                             Service In IT Company                                 
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Designation::</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                             IT & Software Engineering
[ Software Developer / Programmer ]                      
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Industry:</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                             IT - Software/ Software Services                            
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Currency:</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                             USD [United States Dollar - United States of America (USA)]                                     
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Monthly Income:</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                             25000                             
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </div>

                        <div class="lifestyle_section_ttl">
                           <h2 class="font-size-24">Family Information</h2>
                        </div>
                        <div id="about_self_nri" class="detail_section pos_rel">
                           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <div class="row">
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Family Value :</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                            xyzzzzzzz                          
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Family Type :</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                            Nucleur                            
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                       <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                          <p>Family Status: :</p>
                                       </div>
                                       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                          <p class="profile_search_description">
                                            jsfjrds                           
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </div>


                        <div class="background_section_ttl">
                           <h2 class="font-size-24">Short Description About Abhijit & His Interested Profile</h2>
                           <p class="paragraph-text2 black-text">* For your security reasons do not share your name, contact details or social media links below.</p>
                        </div>
                        <div id="about_self_nri" class="detail_section pos_rel">
                           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <div class="row">
                                 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <p>Short Description About Yourself :</p>
                                 </div>
                                 <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <p class="profile_search_description">
                                    </p>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <p>Short Description About Your Family :</p>
                                 </div>
                                 <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <p class="profile_search_description">
                                    </p>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <p>I am looking for someone who is… :</p>
                                 </div>
                                 <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <p class="profile_search_description">
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>