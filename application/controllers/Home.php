<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Home_md');
        $this->load->library('form_validation');
	}
	
	public function pre($data)
	{
		echo "<pre>";
		print_r($data);die;
	}
	public function sql()
	{
		echo $this->db->last_query();die;
		print_r($data);die;
	}
	public function index()
	{
		$countries = $this->Home_md->get_countries();
		$data = [
			'countries'=>$countries
		];
		$this->load->userTemplate('home/index',$data);
	}

	public function about_us(Type $var = null){
		$aboutus = $this->Home_md->aboutus();
		$data = [
			'content'=>$aboutus
		];
		$this->load->userTemplate('home/aboutus',$data);
	}
	public function contact_us(Type $var = null){
		$contact = $this->Home_md->contact();
		$recaptchaAllowed = $this->Home_md->getreCaptchaAllowed(); 
		$data = [
			'result'=>$contact,
			'recaptchaAllowed'=>$recaptchaAllowed
		];
		//$this->pre($data);
		$this->load->userTemplate('home/contactus',$data);
	}
	public function disclaimer(Type $var = null){
		$disclaimer = $this->Home_md->disclaimer();
		$data = [
			'content'=>$disclaimer
		];
		$this->load->userTemplate('home/disclaimer',$data);
	}
	public function privacy_policy(Type $var = null){
		$privacy_policy = $this->Home_md->privacy_policy();
		$data = [
			'content'=>$privacy_policy
		];
		$this->load->userTemplate('home/privacy_policy',$data);
	}
	public function terms_of_service(Type $var = null){
		$terms_of_service = $this->Home_md->terms_of_service();
		$data = [
			'content'=>$terms_of_service
		];
		$this->load->userTemplate('home/terms_of_service',$data);
	}

	public function membership_plan(){
		$this->load->userTemplate('home/membership_plan');
	}
	public function register(){
		$countries = $this->Home_md->get_countries();
		$recaptchaAllowed = $this->Home_md->getreCaptchaAllowed(); 
		$MinimumUserPasswordLength = $this->Home_md->getMinimumUserPasswordLength();
		$data = [
			'countries'=>$countries,
			'MinPassLength' =>$MinimumUserPasswordLength->MinimumUserPasswordLength,
			'MaxPassLength' =>$MinimumUserPasswordLength->MaximumUserPasswordLength,
			'recaptchaAllowed' => $recaptchaAllowed
		];
		$this->load->userTemplate('home/register',$data);
	} 
	
	public function getcity(){
		$state = $this->input->post('state');
		//$countries = $this->Home_md->get_countries();
		
		$city = $this->Home_md->city_get_by_ctiy($state);
		//echo $this->db->last_query();die;
		echo json_encode($city);
	}

	public function state(){
		$state = $this->input->post('state');
		$city = $this->Home_md->city_get_by_ctiy($state);
		$countries = $this->Home_md->get_countries_by_id($state);
		$city = $this->Home_md->city_get_by_state($state);
		$data = [
			'city' => $city,
			'code' => $countries
		];
		//echo $this->db->last_query();die;
		echo json_encode($data);
	}
}
