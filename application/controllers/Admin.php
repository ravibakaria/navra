
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Home_md');
        $this->load->model('Admin_md');
        $this->load->library('form_validation');
        if($this->session->userdata('admin')==''){
			redirect (base_url('admin'));
		}
    }
    public function dashboard()
    {

        $inactiv = $this->Admin_md->getInActiveMemberCount(0);
             // echo $this->db->last_query();die;
        $activ = $this->Admin_md->getInActiveMemberCount(1);
        $deactiv = $this->Admin_md->getInActiveMemberCount(2);
        $sespend = $this->Admin_md->getInActiveMemberCount(3);
        $getBrideCount = $this->Admin_md->getBrideCount(2);
        $getNeverMarrideBrides = $this->Admin_md->getNeverMarrideBrides(2);
        $getProfileIncompleteBrides = $this->Admin_md->getProfileIncompleteBrides(2);
        $getDivorceBrides = $this->Admin_md->getDivorceBrides(2);
        $getWidowedBrides = $this->Admin_md->getWidowedBrides(2);
        $getAnnulledBrides = $this->Admin_md->getAnnulledBrides(2);
        $getAwaitingDivorceBrides = $this->Admin_md->getAwaitingDivorceBrides(2);
     

        $getGroomCount = $this->Admin_md->getBrideCount(1);
        $getNeverMarrideGroom = $this->Admin_md->getNeverMarrideBrides(1);
        $getProfileIncompleteGroom = $this->Admin_md->getProfileIncompleteBrides(1);
        $getDivorceGroom = $this->Admin_md->getDivorceBrides(1);
        $getWidowedGroom = $this->Admin_md->getWidowedBrides(1);
        $getAnnulledGroom = $this->Admin_md->getAnnulledBrides(1);
        $getAwaitingDivorceGroom = $this->Admin_md->getAwaitingDivorceBrides(1);
        
        $getTGenderCount = $this->Admin_md->getBrideCount(3);
       // echo $this->db->last_query();die;
        $getNeverMarrideTGender = $this->Admin_md->getNeverMarrideBrides(3);
        $getProfileIncompleteTGender = $this->Admin_md->getProfileIncompleteBrides(3);
        $getDivorceTGender = $this->Admin_md->getDivorceBrides(3);
        $getWidowedTGender = $this->Admin_md->getWidowedBrides(3);
        $getAnnulledTGender = $this->Admin_md->getAnnulledBrides(3);
        $getAwaitingDivorceTGender = $this->Admin_md->getAwaitingDivorceBrides(3);


        $totalCount = (int)$getBrideCount->bride_count + (int)$getGroomCount->bride_count +  (int)$getTGenderCount->bride_count;
         $getNeverMarridetotal = count($getNeverMarrideBrides) + count($getNeverMarrideGroom) + count($getNeverMarrideTGender);

         $getProfileIncompletetotal = count($getProfileIncompleteBrides) + count($getProfileIncompleteGroom) + count($getProfileIncompleteTGender);

         $getDivorcetotal = count($getDivorceBrides) + count($getDivorceGroom) + count($getDivorceTGender);

         $getWidowedtotal = count($getWidowedBrides) + count($getWidowedGroom) + count($getWidowedTGender);

         $getAnnulledtotal = count($getAnnulledBrides) + count($getAnnulledGroom) + count($getAnnulledTGender);

         $getAwaitingDivorcetotal = count($getAwaitingDivorceBrides) + count($getAwaitingDivorceGroom) + count($getAwaitingDivorceTGender);

        $data = [
            'getBrideCount' =>$getBrideCount,
            'getNeverMarrideBrides' => $getNeverMarrideBrides,
            'getProfileIncompleteBrides' =>$getProfileIncompleteBrides,
            'getDivorceBrides' =>$getDivorceBrides,
            'getWidowedBrides' =>$getWidowedBrides,
            'getAnnulledBrides' =>$getAnnulledBrides,
            'getAwaitingDivorceBrides' => $getAwaitingDivorceBrides,

            'getGroomCount' =>$getGroomCount,
            'getNeverMarrideGroom' => $getNeverMarrideGroom,
            'getProfileIncompleteGroom' =>$getProfileIncompleteGroom,
            'getDivorceGroom' =>$getDivorceGroom,
            'getWidowedGroom' =>$getWidowedGroom,
            'getAnnulledGroom' =>$getAnnulledGroom,
            'getAwaitingDivorceGroom' => $getAwaitingDivorceGroom,

            'getTGenderCount' =>$getTGenderCount,
            'getNeverMarrideTGender' => $getNeverMarrideTGender,
            'getProfileIncompleteTGender' =>$getProfileIncompleteTGender,
            'getDivorceTGender' =>$getDivorceTGender,
            'getWidowedTGender' =>$getWidowedTGender,
            'getAnnulledTGender' =>$getAnnulledTGender,
            'getAwaitingDivorceTGender' => $getAwaitingDivorceTGender,

            'totalCount' =>$totalCount,
            'getNeverMarridetotal' => $getNeverMarridetotal,
            'getProfileIncompletetotal' =>$getProfileIncompletetotal,
            'getDivorcetotal' =>$getDivorcetotal,
            'getWidowedtotal' =>$getWidowedtotal,
            'getAnnulledtotal' =>$getAnnulledtotal,
            'getAwaitingDivorcetotal' => $getAwaitingDivorcetotal,

            'inactiv'=>$inactiv,
            'activ'=>$activ,
            'deactiv'=>$deactiv,
            'sespend'=>$sespend 

        ];
        // echo "<pre>";
        // print_r($data);die;
        $this->load->template('admin/dashboard',$data);
    }

    public function view_members($gender,$marital_status){

        if($gender=='1'){
            $display_gender = "Grooms";
        }
        
        if($gender=='2'){
            $display_gender = "Brides";
        }
       
        if($gender=='3') {
            $display_gender = "Others";
        }
        if($gender=='0') { 
            $display_gender = "All genders";
        }


        if($marital_status=='1'){
            $display_marital_status = "Never Married";
        }
       
        if($marital_status=='2'){
            $display_marital_status = "Awaiting Divorce";
        }
        
        if($marital_status=='3'){
            $display_marital_status = "Divorced";
        }
       
        if($marital_status=='4'){
            $display_marital_status = "Widowed";
        }
       
        if($marital_status=='5'){
            $display_marital_status = "Annulled";
        }
        if($marital_status=='-1'){
            $display_marital_status = "All marital status";
        }
        $data = [
            'gender'=> $gender,
            'marital_status' => $marital_status,
            'display_marital_status' => $display_marital_status,
            'display_gender' => $display_gender
        ];
        $this->session->set_userdata('view_member_gender',$gender);
        $this->session->set_userdata('view_member_marital_status',$marital_status);
        $this->load->template('admin/view_members',$data);

    }

    public function view_members_response(){
        $data = $row = array();
        // print_r($_POST);exit();
         $memData = $this->Admin_md->getRows1($_POST);
         //echo $this->db->last_query();die;
         $i = $_POST['start'];
         foreach($memData as $member){
             $i++;
             $st ='';
             if($member->gender == 1){
                 $st ='Male';
             }
             if($member->gender == 2){
                $st ='FeMale';
            }
            if($member->gender == 3){
                 $st ='Other';
             }




             if($member->marital_status=='1'){
                 $row='Never Married';
             }
            
             if($member->marital_status=='2'){
                 $row='Awaiting Divorce';
             }
             
             if($member->marital_status=='3'){
                 $row='Divorced';
             }
            
             if($member->marital_status=='4'){
                 $row ='Widowed';
             }
             
             if($member->marital_status=='5'){
                 $row='Annulled';
             }
     
            
             if($member->status=='0'){
                 $row1 = "<b style='color:red;'>In-Active</b>";
             }
            
             if($member->status=='1'){
                 $row1 = "<b style='color:green;'>Active</b>";
             }
           
             if($member->status=='2'){
                 $row1 = "<b style='color:blue;'>Deactivate</b>";
             }
            
             if($member->status=='3'){
                 $row1 = "<b style='color:red;'>Suspend</b>";
             }	


            $data[] = array($member->id, $member->firstname, $member->lastname,$member->unique_code,$st,$member->email,$member->phonenumber,$row, $row1);
         }
         
         $output = array(
             "draw" => $_POST['draw'],
             "recordsTotal" => $this->Admin_md->countAll1(),
             "recordsFiltered" => $this->Admin_md->countFiltered1($_POST),
             "data" => $data,
         );
         
         // Output to JSON format
         echo json_encode($output);
    }
}