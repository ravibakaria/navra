<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		if($this->session->userdata('user')==''){
			redirect (base_url('login'));
		}
		$this->load->model('User_md');
		$this->load->model('Home_md');
        $this->load->library('form_validation');
	}
	
	public function pre($data)
	{
		echo "<pre>";
		print_r($data);die;
	}
	public function sql()
	{
		echo $this->db->last_query();die;
		print_r($data);die;
	}
	public function index()
	{
		$id = $this->session->userdata('user')->id;
		$uniq_id = $this->session->userdata('user')->unique_code;
		$countries = $this->User_md->get_countries();
		$ProfileViewCount = $this->User_md->getProfileViewCount($id,$uniq_id);
		$membership = $this->User_md->getmembership($id);
		$getMemberIsFeaturedOrNot = $this->User_md->getMemberIsFeaturedOrNot($id);
		$getUserInterestReceived = $this->User_md->getUserInterestReceived($id);
		$getUsershortlistedProfiles = $this->User_md->getUsershortlistedProfiles($id);
		$getUsershortlistedYourProfile = $this->User_md->getUsershortlistedYourProfile($id);
		$getUserMatchCount = $this->User_md->getUserMatchCount($id);
		$getUserNewMessage = $this->User_md->getUserNewMessage($id);
		$getUserInterestSent = $this->User_md->getUserInterestSent($id);
		//$this->sql();
		$data = [
			'countries'=>$countries,
			'ProfileViewCount' =>$ProfileViewCount,
			'membership' => $membership,
			'getMemberIsFeaturedOrNot' =>$getMemberIsFeaturedOrNot,
			'getUserInterestReceived' => $getUserInterestReceived,
			'getUsershortlistedProfiles' =>$getUsershortlistedProfiles,
			'getUsershortlistedYourProfile' => $getUsershortlistedYourProfile,
			'getUserMatchCount' =>$getUserMatchCount,
			'getUserNewMessage' => $getUserNewMessage,
			'getUserInterestSent' =>$getUserInterestSent
		];
		//$this->pre($getUserMatchCount);
		$this->load->userloginTemplate('user/dashbord',$data);
	}

	public function logout(){
		$this->session->unset_userdata('user');
		$this->session->sess_destroy();
		redirect (base_url('login'));
	}

	

	public function myorder(){
		
		$id = $this->session->userdata('user')->id;
		$membership = $this->User_md->getmembershipd($id);
		$DefaultCurrency = $this->User_md->getDefaultCurrency();
		$DefaultCurrencyCode = $this->User_md->getDefaultCurrencyCode($DefaultCurrency);
		$data = [
			'result_order_list' => $membership,
			'DefaultCurrency' =>$DefaultCurrency,
			'DefaultCurrencyCode'=>$DefaultCurrencyCode
		];
		$this->load->userloginTemplate('user/myorder',$data);
	}

	public function order_receipt(){
		$id = $this->session->userdata('user')->id;
		$receipt_id=$this->input->post('receipt_id');
		$stmt_get_pp_details = $this->User_md->order_receipt($receipt_id,$id);
		$DefaultCurrency = $this->User_md->getDefaultCurrency();
		$DefaultCurrencyCode = $this->User_md->getDefaultCurrencyCode($DefaultCurrency);
		$PhoneCode = $this->User_md->getUserCountryPhoneCode($id);
		$contact = $this->Home_md->contact();
		
		$data = [
			'result_get_pp_details' => $stmt_get_pp_details,
			'DefaultCurrency' =>$DefaultCurrency,
			'DefaultCurrencyCode'=>$DefaultCurrencyCode,
			'PhoneCode'=>$PhoneCode,
			'result' =>$contact
		];
		
		$this->load->view('user/order_receipt',$data);
		
	}

	public function my_matches(){
		$id = $this->session->userdata('user')->id;
		$membership = $this->User_md->getmembershipd($id);
		$DefaultCurrency = $this->User_md->getDefaultCurrency();
		$DefaultCurrencyCode = $this->User_md->getDefaultCurrencyCode($DefaultCurrency);
		$data = [
			'result_order_list' => $membership,
			'DefaultCurrency' =>$DefaultCurrency,
			'DefaultCurrencyCode'=>$DefaultCurrencyCode
		];
		$this->load->userloginTemplate('user/my_matches',$data);
	}

	public function interested(){
		$id = $this->session->userdata('user')->id;
		$membership = $this->User_md->getmembershipd($id);
		$DefaultCurrency = $this->User_md->getDefaultCurrency();
		$DefaultCurrencyCode = $this->User_md->getDefaultCurrencyCode($DefaultCurrency);
		$data = [
			'result_order_list' => $membership,
			'DefaultCurrency' =>$DefaultCurrency,
			'DefaultCurrencyCode'=>$DefaultCurrencyCode,
			'interest' => $_GET['interest']
		];
		$this->load->userloginTemplate('user/interested',$data);
	}

	public function interest_profiles_helper(){
		// print_r($this->input->post());
		echo "<h2>Work in prosses</h2>";
		//$this->load->userloginTemplate('user/my_matches');
	}

	public function shortlisted_profiles(){
		$value = $_GET['shortlist'];
		//echo "<h2>Work in prosses</h2>";
		$this->load->userloginTemplate('user/my_matches');
	}

	public function my_profile(){
		$id = $this->session->userdata('user')->id;
		$this->load->userloginTemplate('user/my_profile');
	}
	
}
?>