<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		$this->load->model('Login_md');
		$this->load->model('Home_md');
		$this->load->library('form_validation');
		if($this->session->userdata('user') !=''){
			redirect (base_url('user'));
		}
	}
	
	public function index1()
	{
		$this->load->view('welcome_message');
	}
	public function index()
	{
		$recaptchaAllowed = $this->Home_md->getreCaptchaAllowed(); 
		// $countries = $this->Login_md->get_countries();
		$data = [
			'recaptchaAllowed'=>$recaptchaAllowed
		];
		
		$this->load->userTemplate('login/login',$data);
	}
	function quote_smart($value){
        $value = trim(htmlentities(strip_tags($value)));    
        if ( get_magic_quotes_gpc() ){
            $value = stripslashes( $value );
        }
        return $value;
	}
	
	public function userlogin(){
		$recaptchaAllowed = $this->Home_md->getreCaptchaAllowed();
		$IP_Address = $_SERVER['REMOTE_ADDR'];
		if($recaptchaAllowed->recaptchaAllowed == "1"){
			if($this->input->post('g-recaptcha-response') && !empty($this->input->post('g-recaptcha-response'))){
			//your site secret key
				$secret = $recaptchaAllowed->reCaptchaSecretKey;
				$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
				$responseData = json_decode($verifyResponse);
				if($responseData->success){
					if (isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['password']) && $_POST['password'] != ''){
						$email = $this->quote_smart($_POST['email']);
						$password = md5($this->quote_smart($_POST['password']));
						$var = $this->Login_md->login('clients',$email);
						if(!empty($var)){
							if($var->status=='1' || $var->status=='2'){
								if($var->isEmailVerified=='1'){
									if($var->password == $password){
										$sql = "INSERT INTO `userlogs`(`userid`,`user_firstname`,`user_type`,`IP_Address`,`loggedOn`) VALUES('$var->id','$var->firstname','Client','$IP_Address',now())";
										$query = $this->db->query($sql);
										$sql1 = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$var->id','login','to system','$IP_Address',now())";
										$query = $this->db->query($sql);
										$this->session->set_userdata('user',$var);
										redirect(base_url('user'));
										
									}else{
										$this->session->set_flashdata('message_e', 'Password invalid');
										
										redirect(base_url('login'));
									}
								}else if($var->isEmailVerified=='0'){       
									$errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email id.<br/>
											<a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
									$this->session->set_flashdata('message_e', $errorMessage);
									redirect(base_url('login'));
								}
								
						}else if($var->status=='0'){
							$errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email address.<br/>
								<a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
								$this->session->set_flashdata('message_e', $errorMessage);
							redirect(base_url('login'));
						} else if($var->status=='3'){
							$errorMessage = "Your account is suspended.<br/>Contact administrator to activate your account.";
							$this->session->set_flashdata('message_e', $errorMessage);
							redirect(base_url('login'));
						}
							
						}else{
							$this->session->set_flashdata('message_e', 'invalid Password Or Email');
							redirect(base_url('login'));
						}		
					}
				}
			}
		}else {
			if (isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['password']) && $_POST['password'] != ''){
				$email = $this->quote_smart($_POST['email']);
				$password = md5($this->quote_smart($_POST['password']));
				$var = $this->Login_md->login('clients',$email);
				if(!empty($var)){
					if($var->status=='1' || $var->status=='2'){
						if($var->isEmailVerified=='1'){
							if($var->password == md5($password)){
								$sql = "INSERT INTO `userlogs`(`userid`,`user_firstname`,`user_type`,`IP_Address`,`loggedOn`) VALUES('$var->id','$var->firstname','Client','$IP_Address',now())";
								$query = $this->db->query($sql);
								$sql1 = "INSERT INTO member_activity_logs(userid,task,activity,IP_Address,created_On) VALUES('$var->id','login','to system','$IP_Address',now())";
								$query = $this->db->query($sql);
								$this->session->set_userdata('user',$var);
								redirect(base_url('user'));
								
							}else{
								$this->session->set_flashdata('message_e', 'Password invalid');
								
								redirect(base_url('login'));
							}
						}else if($var->isEmailVerified=='0'){       
							$errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email id.<br/>
									<a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
							$this->session->set_flashdata('message_e', $errorMessage);
							redirect(base_url('login'));
						}
					}else if($var->status=='0'){
						$errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email address.<br/>
						<a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
						$this->session->set_flashdata('message_e', $errorMessage);
						redirect(base_url('login'));
					} else if($var->status=='3'){
						$errorMessage = "Your account is suspended.<br/>Contact administrator to activate your account.";
						$this->session->set_flashdata('message_e', $errorMessage);
						redirect(base_url('login'));
						}	
								
				}else{
					$this->session->set_flashdata('message_e', 'invalid Password Or Email');
					redirect(base_url('login'));
				}			
			}		
		}
	}
	
	public function adminlogin(){
		$recaptchaAllowed = $this->Home_md->getreCaptchaAllowed(); 
		// $countries = $this->Login_md->get_countries();
		$data = [
			'recaptchaAllowed'=>$recaptchaAllowed
		];
		
		$this->load->view('admin/login',$data);
	}

	public function adminlogin_action(Type $var = null){
		$recaptchaAllowed = $this->Home_md->getreCaptchaAllowed();
		$IP_Address = $_SERVER['REMOTE_ADDR'];
		if($recaptchaAllowed->recaptchaAllowed == "1"){
			if($this->input->post('g-recaptcha-response') && !empty($this->input->post('g-recaptcha-response'))){
			//your site secret key
				$secret = $recaptchaAllowed->reCaptchaSecretKey;
				$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
				$responseData = json_decode($verifyResponse);
				if($responseData->success){
					if (isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['password']) && $_POST['password'] != ''){
						$email = $this->quote_smart($_POST['email']);
						$password = md5($this->quote_smart($_POST['password']));
						$var = $this->Login_md->adminlogin('admins',$email);
						if(!empty($var)){
							if($var->status=='1'){
								
									if($var->password == $password){
										$sql = "INSERT INTO `userlogs`(`userid`,`user_firstname`,`user_type`,`IP_Address`,`loggedOn`) VALUES('$var->id','$var->firstname','Client','$IP_Address',now())";
										$query = $this->db->query($sql);
										
										$this->session->set_userdata('admin',$var);
										redirect(base_url('admin/dashboard'));
										
									}else{
										$this->session->set_flashdata('message_e', 'Password invalid');
										
										redirect(base_url('admin'));
									}
							
								
						}else if($var->status=='0'){
							$errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email address.<br/>
								<a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
								$this->session->set_flashdata('message_e', $errorMessage);
							redirect(base_url('login'));
						} else if($var->status=='3'){
							$errorMessage = "Your account is suspended.<br/>Contact administrator to activate your account.";
							$this->session->set_flashdata('message_e', $errorMessage);
							redirect(base_url('admin'));
						}
							
						}else{
							$this->session->set_flashdata('message_e', 'invalid Password Or Email');
							redirect(base_url('admin'));
						}		
					}
				}
			}
		}else {
			if (isset($_POST['email']) && $_POST['email'] != '' && isset($_POST['password']) && $_POST['password'] != ''){
				$email = $this->quote_smart($_POST['email']);
				$password = md5($this->quote_smart($_POST['password']));
				$var = $this->Login_md->adminlogin('admins',$email);
				if(!empty($var)){
					if($var->status=='1' ){
					
							if($var->password == md5($password)){
								$sql = "INSERT INTO `userlogs`(`userid`,`user_firstname`,`user_type`,`IP_Address`,`loggedOn`) VALUES('$var->id','$var->firstname','Client','$IP_Address',now())";
								$query = $this->db->query($sql);
								
								$this->session->set_userdata('admin',$var);
								redirect(base_url('admin/dashboard'));
								
							}else{
								$this->session->set_flashdata('message_e', 'Password invalid');
								
								redirect(base_url('admin'));
							}
						
					}else if($var->status=='0'){
						$errorMessage = "Your account is inactive.<br/>Please verify your email inbox/spam & verify your email address.<br/>
						<a class='btn btn-danger' href='send_email_verification.php?email=$email'>Resend Verification Link</a>";
						$this->session->set_flashdata('message_e', $errorMessage);
						redirect(base_url('admin'));
					} else if($var->status=='3'){
						$errorMessage = "Your account is suspended.<br/>Contact administrator to activate your account.";
						$this->session->set_flashdata('message_e', $errorMessage);
						redirect(base_url('admin'));
						}	
								
				}else{
					$this->session->set_flashdata('message_e', 'invalid Password Or Email');
					redirect(base_url('admin'));
				}			
			}		
		}
	}

	public function adminlogout(){
		$this->session->unset_userdata('admin');
		$this->session->sess_destroy();
		redirect (base_url('admin'));
	}
}

?>
