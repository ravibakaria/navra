<?php

class Admin_md extends CI_Model
{
   
    function __construct()
    {
        $this->column_order1 = array( 
            0 =>'A.id',
            1 =>'A.firstname',
            2 =>'A.lastname',
            3 =>'A.unique_code',
            4 =>'A.gender',
            5 =>'A.email',
            6 =>'A.phonenumber',
            7 =>'B.marital_status',
            8 =>'A.status'
        );
        $this->column_search1 = array( 
            0 =>'A.id',
            1 =>'A.firstname',
            2 =>'A.lastname',
            3 =>'A.unique_code',
            4 =>'A.gender',
            5 =>'A.email',
            6 =>'A.phonenumber',
            7 =>'B.marital_status',
            8 =>'A.status'
        );
        $this->order = array('id' => 'asc');
    }
    public function get_countries(){
        $this->db->where('status',1);
        $query = $this->db->get('countries');
        return $query->result();
    }
    public function aboutus(){
      
        $query = $this->db->get('about_us');
        return $query->row();
    }
    public function contact(){
      
        $this->db->select('contact_us.*,cities.name as cityname,states.name as statename,countries.name as countryname');
        $this->db->from('contact_us');
        $this->db->join('states', 'states.id = contact_us.state', 'left'); 
        $this->db->join('cities', 'cities.id =contact_us.city', 'left'); 
        $this->db->join('countries', 'countries.id =contact_us.country', 'left'); 
        $query = $this->db->get();
        return $query->row();
    }
    public function getreCaptchaAllowed(){
       
        $query = $this->db->get('generalsecurity');
        return $query->row();
    }
    public function disclaimer(){
       
        $query = $this->db->get('disclaimer');
        return $query->row();
    }
    public function privacy_policy(){ 
       
        $query = $this->db->get('privacy_policy');
        return $query->row();
    }
    public function terms_of_service(){ 
       
        $query = $this->db->get('terms_of_service');
        return $query->row();
    }

    function getMinimumUserPasswordLength()
    {
       
        $sql = "SELECT `MinimumUserPasswordLength` FROM `generalsecurity`";
        $query = $this->db->get('generalsecurity');
        return $query->row();
    }
    function city_get_by_state($id) { 
        
        $this->db->select('*');
        $this->db->where('country_id', $id); 
        return $this->db->get('states')->result(); 
    } 

    function city_get_by_ctiy($id) { 
       
        $this->db->select('*');
        $this->db->where('state_id', $id); 
        return $this->db->get('cities')->result(); 
    } 
    public function get_countries_by_id($id){
        $this->db->select('phonecode');
        $this->db->where('status',1);
        $this->db->where('id',$id);
        $query = $this->db->get('countries');
        return $query->row();
    }
    function getBrideCount($g)
    {
        $this->db->select(" COUNT(`id`) AS bride_count");
        $this->db->from('clients');
        $this->db->where('gender', $g); 
        
        $query = $this->db->get();
        return $query->row();
       
    }

    /************   Get total NeverMarrideBrides   *************/
    function getNeverMarrideBrides($g)
    {
        $this->db->select('A.`id`,B.`userid`');
        $this->db->from('clients AS A');
        $this->db->join('profilebasic` AS B', 'A.id=B.userid', 'left'); 
        $this->db->where('B.marital_status','1'); 
        $this->db->where('A.gender', $g); 
        $query = $this->db->get();
        return $query->result();
       
    }

    /************   Get total AwaitingDivorceBrides   *************/
    function getAwaitingDivorceBrides($g)
    {
        $this->db->select('A.`id`,B.`userid`');
        $this->db->from('clients AS A');
        $this->db->join('profilebasic` AS B', 'A.id=B.userid', 'left'); 
        $this->db->where('B.marital_status','2'); 
        $this->db->where('A.gender', $g); 
        $query = $this->db->get();
        return $query->result();
        

    }

    /************   Get total DivorceBrides   *************/
    function getDivorceBrides($g)
    {
        $this->db->select('A.`id`,B.`userid`');
        $this->db->from('clients AS A');
        $this->db->join('profilebasic` AS B', 'A.id=B.userid', 'left'); 
        $this->db->where('B.marital_status','3'); 
        $this->db->where('A.gender', $g); 
        $query = $this->db->get();
        return $query->result();
    }

    /************   Get total WidowedBrides   *************/
    function getWidowedBrides($g)
    {
        $this->db->select('A.`id`,B.`userid`');
        $this->db->from('clients AS A');
        $this->db->join('profilebasic` AS B', 'A.id=B.userid', 'left'); 
        $this->db->where('B.marital_status','4'); 
        $this->db->where('A.gender', $g); 
        $query = $this->db->get();
        return $query->result();
    }

    /************   Get total AnnulledBrides   *************/
    function getAnnulledBrides($g)
    {
        $this->db->select('A.`id`,B.`userid`');
        $this->db->from('clients AS A');
        $this->db->join('profilebasic` AS B', 'A.id=B.userid', 'left'); 
        $this->db->where('B.marital_status','5'); 
        $this->db->where('A.gender', $g); 
        $query = $this->db->get();
        return $query->result();
    }

    /************   Get total ProfileIncompleteBrides   *************/
    function getProfileIncompleteBrides($g)
    {
        $sql = "SELECT `id` FROM `clients` WHERE `gender`='$g' AND `id` NOT IN (SELECT `userid` FROM `profilebasic`)";
        //return $this->db->query($sql, $where_condition)->result();
        $query = $this->db->query($sql);
        return $query->result();
        // global $link;
        // $stmt = $link->prepare("SELECT `id` FROM `clients` WHERE `gender`='2' AND `id` NOT IN (SELECT `userid` FROM `profilebasic`)"); 
        // $stmt->execute();
        // $count=$stmt->rowCount();
        // $result = $stmt->fetch();

        // $profileincomplete_bride_count = $count;
        // return $profileincomplete_bride_count;
    }

   
    function getInActiveMemberCount($status)
    {
        $this->db->where('status', $status); 
        $query = $this->db->get('clients');
        return $query->num_rows();
    }

    public function getRows1($postData){
        $this->_get_datatables_query1($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
       // echo $this->db->last_query();exit();
        return $query->result();
    }

    public function countAll1(){
        $gender = $this->session->userdata('view_member_gender');
	    $marital_status = $this->session->userdata('view_member_marital_status');
       
        $this->db->select('A.id,A.firstname,A.lastname,A.unique_code,A.gender,A.email,A.phonenumber,B.marital_status,A.status');
        $this->db->from('clients  AS A');
        $this->db->join('profilebasic AS B', 'A.id=B.userid', 'left'); 

        if($gender=='0'){
		    //$where .= "";
	    }else{
            $this->db->where('A.gender',$gender);
	    }

	    if($marital_status=='0'){
		   // $where .= "";
	    }else if($marital_status=='-1'){
            $where = "A.id NOT IN(SELECT userid FROM profilebasic)";
            $this->db->where($where);
        }else{
            $this->db->where('B.marital_status',$marital_status);
	    }
       
       
      
         return $this->db->count_all_results();
    
    }
    
    public function countFiltered1($postData){
        $this->_get_datatables_query1($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query1($postData){
        $gender = $this->session->userdata('view_member_gender');
	    $marital_status = $this->session->userdata('view_member_marital_status');
       
        $this->db->select('A.id,A.firstname,A.lastname,A.unique_code,A.gender,A.email,A.phonenumber,B.marital_status,A.status');
        $this->db->from('clients  AS A');
        $this->db->join('profilebasic AS B', 'A.id=B.userid', 'left'); 
        
        if($gender=='0'){
		    //$where .= "";
	    }else{
            $this->db->where('A.gender',$gender);
	    }

	    if($marital_status=='0'){
		   // $where .= "";
	    }else if($marital_status=='-1'){
            $where = "A.id NOT IN(SELECT userid FROM profilebasic)";
            $this->db->where($where);
        }else{
            $this->db->where('B.marital_status',$marital_status);
	    }
         
        $i = 0;
        // loop searchable columns 
        foreach($this->column_search1 as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search1) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order1[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
       
    }


    public function getRows($postData){
        $this->_get_datatables_query($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
       // echo $this->db->last_query();exit();
        return $query->result();
    }

    public function countAll(){
        $gender = $this->session->userdata('view_member_gender');
	    $marital_status = $this->session->userdata('view_member_marital_status');
       
        $this->db->select('A.id,A.firstname,A.lastname,A.unique_code,A.gender,A.email,A.phonenumber,B.marital_status,A.status');
        $this->db->from('clients  AS A');
        $this->db->join('profilebasic AS B', 'A.id=B.userid', 'left'); 

        if($gender=='0'){
		    //$where .= "";
	    }else{
            $this->db->where('A.gender',$gender);
	    }

	    if($marital_status=='0'){
		   // $where .= "";
	    }else if($marital_status=='-1'){
            $where = "A.id NOT IN(SELECT userid FROM profilebasic)";
            $this->db->where($where);
        }else{
            $this->db->where('B.marital_status',$marital_status);
	    }
       
       
      
         return $this->db->count_all_results();
    
    }
    
    public function countFiltered($postData){
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query($postData){
        $gender = $this->session->userdata('view_member_gender');
	    $marital_status = $this->session->userdata('view_member_marital_status');
       
        $this->db->select('A.id,A.firstname,A.lastname,A.unique_code,A.gender,A.email,A.phonenumber,B.marital_status,A.status');
        $this->db->from('clients  AS A');
        $this->db->join('profilebasic AS B', 'A.id=B.userid', 'left'); 
        
        if($gender=='0'){
		    //$where .= "";
	    }else{
            $this->db->where('A.gender',$gender);
	    }

	    if($marital_status=='0'){
		   // $where .= "";
	    }else if($marital_status=='-1'){
            $where = "A.id NOT IN(SELECT userid FROM profilebasic)";
            $this->db->where($where);
        }else{
            $this->db->where('B.marital_status',$marital_status);
	    }
         
        $i = 0;
        // loop searchable columns 
        foreach($this->column_search1 as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search1) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order1[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
       
    }

}
?>