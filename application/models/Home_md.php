<?php

class Home_md extends CI_Model
{
   
    function __construct()
    {
        
    }
    public function get_countries(){
        $this->db->where('status',1);
        $query = $this->db->get('countries');
        return $query->result();
    }
    public function aboutus(){
      
        $query = $this->db->get('about_us');
        return $query->row();
    }
    public function contact(){
      
        $this->db->select('contact_us.*,cities.name as cityname,states.name as statename,countries.name as countryname');
        $this->db->from('contact_us');
        $this->db->join('states', 'states.id = contact_us.state', 'left'); 
        $this->db->join('cities', 'cities.id =contact_us.city', 'left'); 
        $this->db->join('countries', 'countries.id =contact_us.country', 'left'); 
        $query = $this->db->get();
        return $query->row();
    }
    public function getreCaptchaAllowed(){
       
        $query = $this->db->get('generalsecurity');
        return $query->row();
    }
    public function disclaimer(){
       
        $query = $this->db->get('disclaimer');
        return $query->row();
    }
    public function privacy_policy(){ 
       
        $query = $this->db->get('privacy_policy');
        return $query->row();
    }
    public function terms_of_service(){ 
       
        $query = $this->db->get('terms_of_service');
        return $query->row();
    }

    function getMinimumUserPasswordLength()
    {
       
        $sql = "SELECT `MinimumUserPasswordLength` FROM `generalsecurity`";
        $query = $this->db->get('generalsecurity');
        return $query->row();
    }
    function city_get_by_state($id) { 
        
        $this->db->select('*');
        $this->db->where('country_id', $id); 
        return $this->db->get('states')->result(); 
    } 

    function city_get_by_ctiy($id) { 
       
        $this->db->select('*');
        $this->db->where('state_id', $id); 
        return $this->db->get('cities')->result(); 
    } 
    public function get_countries_by_id($id){
        $this->db->select('phonecode');
        $this->db->where('status',1);
        $this->db->where('id',$id);
        $query = $this->db->get('countries');
        return $query->row();
    }
   


}
?>