<?php

class Login_md extends CI_Model
{
   
    function __construct()
    {
        
    }
    public function get_countries(){
        $this->db->where('status',1);
        $query = $this->db->get('countries');
        return $query->result();
    }
    public function login($table,$email){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('email',$email);
        //$this->db->where('isdelete',0);
        $prevQuery = $this->db->get();
        return $prevQuery->row();
    }
    public function adminlogin($table,$email){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('username',$email);
        //$this->db->where('isdelete',0);
        $prevQuery = $this->db->get();
        return $prevQuery->row();
    }

}
?>