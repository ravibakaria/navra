<?php

class User_md extends CI_Model
{
   
    function __construct()
    {
        
    }
    public function get_countries(){
        $this->db->where('status',1);
        $query = $this->db->get('countries');
        return $query->result();
    }
    
    function getProfileViewCount($id,$profileid)
    {
        $sql_like_profile_view = "SELECT * FROM `profile_views` WHERE `userid`='$id' AND unique_code='$profileid'";
        return $this->db->query($sql_like_profile_view )->result(); 

    }
    function getmembership($id)
    {   $sql="SELECT * FROM `payment_transactions` WHERE userid='$id' AND status='1'";
        //$sql_like_profile_view = "SELECT * FROM `payment_transactions` WHERE userid='$id' AND membership_plan='Yes'";
        return $this->db->query($sql)->result(); 
    }
    function getmembershipd($id)
    {  // $sql="SELECT * FROM `payment_transactions` WHERE userid='$id' AND status='1'";
        $sql_like_profile_view = "SELECT * FROM payment_transactions WHERE userid='$id' ORDER BY created_at DESC";
        return $this->db->query($sql_like_profile_view)->result_array(); 
    }
    function getDefaultCurrency()
    {
        $sql = "SELECT `DefaultCurrency` FROM `localizationsetup`";
        $result = $this->db->query($sql)->row(); 
        // echo "<pre>";
        // print_r($result);die;
        $DefaultCurrency = $result->DefaultCurrency;
        if($DefaultCurrency=='' || $DefaultCurrency==null)
        {
            $DefaultCurrency = 'United States Dollar';
        }
        return $DefaultCurrency;
    }
    function getDefaultCurrencyCode($x)
    {
       
        $sql = "SELECT `code` FROM `currency` WHERE currency='$x'";
        $result = $this->db->query($sql)->row(); 
        // echo "<pre>";
        // print_r($result);die;
        $code = $result->code;
        if($code=='' || $code==null)
        {
            $code = 'USD';
        }
        return $code;
    }
    function order_receipt($receipt_id,$user_id)
    {  // $sql="SELECT * FROM `payment_transactions` WHERE userid='$id' AND status='1'";
        $sql_like_profile_view = "SELECT * FROM `payment_transactions` WHERE `id`='$receipt_id' AND userid='$user_id'";
        return $this->db->query($sql_like_profile_view)->row(); 
    }
    function getUserCountryPhoneCode($id)
    {
        $res_count  = "SELECT phonecode FROM `clients` WHERE `id`='$id'";
        $row_data=$this->db->query($res_count)->row(); 
        $phonecode = $row_data->phonecode;
        return $phonecode;
    }
    function getMemberIsFeaturedOrNot($id){
        $stmt = "SELECT * FROM `payment_transactions` WHERE userid='$id' AND featured_listing='Yes' AND status='1' ORDER BY id DESC"; 
        return $this->db->query($stmt)->result_array(); 
    }
    function getUserInterestReceived($id){
        $stmt = "SELECT * FROM `interested` WHERE `inter_id`='$id'"; 
        return $this->db->query($stmt)->result_array(); 
    }
    function getUsershortlistedProfiles($x)
    {
        $stmt = "SELECT * FROM `shortlisted` WHERE `userid`='$x'"; 
        return $this->db->query($stmt)->result_array(); 
   
    }
    function getUsershortlistedYourProfile($x)
    {
      
        $stmt = "SELECT * FROM `shortlisted` WHERE `short_id`='$x'"; 
        return $this->db->query($stmt)->result_array(); 
    }
    function getUserMatchCount($x)
    {
        
        $where=null;
        $sqlRec=null;
        $sql_search_preference = "SELECT * FROM preferences WHERE userid='$x'";
        $search_preference = $this->db->query($sql_search_preference)->row(); 
        if(!empty($count))
        {
            $gender = $search_preference->gender;
            $marital_status = $search_preference->marital_status;
            $city = $search_preference->city;
            $state = $search_preference->state;
            $country = $search_preference->country;
            $religion = $search_preference->religion;
            $mother_tongue = $search_preference->mother_tongue;
            $caste = $search_preference->caste;
            $education = $search_preference->education;
            $occupation = $search_preference->occupation;
            $currency = $search_preference->currency;
            $monthly_income_from = $search_preference->monthly_income_from;
            $monthly_income_to = $search_preference->monthly_income_to;
            $from_date = $search_preference->from_date;
            $to_date = $search_preference->to_date;
            $body_type = $search_preference->body_type;
            $complexion = $search_preference->complexion;
            $height_from = $search_preference->height_from;
            $height_to = $search_preference->height_to;
            $fam_type = $search_preference->fam_type;
            $smoke_habbit = $search_preference->smoke_habbit;
            $drink_habbit = $search_preference->drink_habbit;
            $eat_habbit = $search_preference->eat_habbit;

            $sql = "SELECT A.*,A.id as idx,B.*,C.*,D.*,E.*,F.photo,G.* FROM 
            clients as A 
            LEFT OUTER JOIN
            profilebasic AS B ON A.id = B.userid
            LEFT OUTER JOIN
            address AS C ON A.id = C.userid
            LEFT OUTER JOIN
            profilereligion AS D ON A.id = D.userid
            LEFT OUTER JOIN
            eduocc AS E ON A.id = E.userid
            LEFT OUTER JOIN
            profilepic AS F ON A.id = F.userid
            LEFT OUTER JOIN
            family AS G ON A.id = G.userid
            WHERE A.gender='$gender' ";

            /******  Profile Client Start *****/
                if($from_date=='0')      // Age form
                {
                    $where .="";
                }
                else
                {
                    $age_from = date('Y-m-d', strtotime('-'.$from_date.' years'));
                    $where .=" AND A.dob >='$age_from'";
                }

                if($to_date=='0')      // Age to
                {
                    $where .="";
                }
                else
                {
                    $age_from_chk = strtotime($age_from);
                    $age_to = date('Y-m-d',strtotime("+".$to_date." years",$age_from_chk));
                    $where .=" AND A.dob <='$age_to'";
                }
            /******  Profile Client End *****/

            /******  Profile Basic Start *****/
                if($marital_status=='0' || $marital_status=='' || $marital_status==null)         // Marital status
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.marital_status IN($marital_status)";
                }

                if($body_type=='0' || $body_type=='-1')         // body_type
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.body_type ='$body_type'";
                }

                if($complexion=='0' || $complexion=='-1')         // complexion
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.complexion ='$complexion'";
                }

                if($height_from=='0')         // height_from
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.height_from >='$height_from'";
                }

                if($height_to=='0')         // height_to
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.height_to <='$height_to'";
                }

                if($fam_type=='0' || $fam_type=='-1')         // fam_type
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.fam_type ='$fam_type'";
                }

                if($smoke_habbit=='0' || $smoke_habbit=='-1')         // smoke_habbit
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.smoke_habbit ='$smoke_habbit'";
                }

                if($drink_habbit=='0' || $drink_habbit=='-1')         // drink_habbit
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.drink_habbit ='$drink_habbit'";
                }

                if($eat_habbit=='0' || $eat_habbit=='-1')         // eat_habbit
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.eat_habbit ='$eat_habbit'";
                }

                if($eat_habbit=='0' || $eat_habbit=='-1')         // eat_habbit
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND B.eat_habbit ='$eat_habbit'";
                }
            /******  Profile Basic End *****/


            /******  Profile location Start *****/
                if($city=='0' || $city=='-1')    // City
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND C.city ='$city'";
                }

                if($state=='0' || $state=='-1')     //state
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND C.state ='$state'";
                }

                if($country=='0' || $country=='-1')     //country
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND C.country ='$country'";
                }
            /******  Profile location End *****/


            /******  Profile Education Start *****/
                if($education=='0' || $education=='-1')     //country
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND E.education ='$education'";
                }

                if($occupation=='0' || $occupation=='-1')     //occupation
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND E.occupation ='$occupation'";
                }

                if($currency=='0')     //currency
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND E.currency ='$currency'";
                }

                if($monthly_income_from=='' || $monthly_income_from==null)     //monthly_income_from
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND E.monthly_income_from >='$monthly_income_from'";
                }

                if($monthly_income_to=='' || $monthly_income_to==null)     //monthly_income_to
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND E.monthly_income_to <='$monthly_income_to'";
                }
            /******  Profile Education  *****/


            /******  Profile Relegious Start *****/
                if($religion=='0' || $religion=='-1')     //religion
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND D.country ='$religion'";
                }

                if($mother_tongue=='0' || $mother_tongue=='-1')     //mother_tongue
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND D.mother_tongue ='$mother_tongue'";
                }

                if($caste=='0' || $caste=='-1')     //caste
                {
                    $where .="";
                }
                else
                {
                    $where .=" AND D.caste ='$caste'";
                }

            /******  Profile Relegious End *****/
            
            $where .=" ORDER BY rand()";
            
            $sqlRec .=  $sql.$where;
            // echo $sqlRec; die;
            $sqlTot   = $this->db->query($sqlRec)->num_rows(); 
           
           
        }else{
            $sqlTot = '0';
        }    
        return $sqlTot;
    }

    function getUserNewMessage($x)
    {
        $Second_last_login = $this->getSecondLastUserLoginDate($x);
        $stmt = "SELECT * FROM `messagechat` WHERE `userTo`='$x' AND (messagedOn>='$Second_last_login')"; 
        $count= $this->db->query($stmt)->num_rows();
        return $count;
    }

    function getSecondLastUserLoginDate($x)
    {  
        $stmt = "SELECT MAX(loggedOn) AS loggedOn FROM userlogs WHERE userid='$x' AND loggedOn < (SELECT MAX(loggedOn) FROM userlogs WHERE userid='$x')";    
        $result = $this->db->query($stmt)->row();  
        $secondlastLoginTime = $result->loggedOn;
        return $secondlastLoginTime;
         
    }
    function getUserInterestSent($x)
    {
      
        $stmt = "SELECT * FROM `interested` WHERE `userid`='$x'"; 
        $count= $this->db->query($stmt)->num_rows();
        return $count;
    }
}
?>