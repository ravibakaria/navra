<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['free-register']['GET'] = 'home/register';
$route['About-us']['GET'] = 'home/about_us';
$route['Contact-us']['GET'] = 'home/contact_us';
$route['Disclaimer']['GET'] = 'home/disclaimer';
$route['Privacy-Policy']['GET'] = 'home/privacy_policy';
$route['Terms-Of-Service']['GET'] = 'home/terms_of_service';
$route['About-us']['GET'] = 'home/about_us';
$route['users/my-orders']['GET'] = 'user/myorder';
$route['users/my-matches']['GET'] = 'user/my_matches';
$route['users/interested-prospects']['GET'] = 'user/interested';
$route['users/shortlisted-profiles']['GET'] = 'user/shortlisted_profiles';
$route['users/my-profile']['GET'] = 'user/my_profile';
$route['users/Membership-Plans']['GET'] = 'home/membership_plan';
$route['admin']['GET'] = 'login/adminlogin';

