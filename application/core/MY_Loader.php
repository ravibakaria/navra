<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {

    private $CI;
    private $roleId;
    private $roleName;
    
    function __construct() {
        parent::__construct();
        $this->CI = &get_instance();
    }
    public function template($template_name, $vars = array(), $return = FALSE)
    {   

        if($return):
            $content  = $this->view('admin/adminheader', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('admin/adminfooter', $vars, $return);

            return $content;
        else:
            $this->view('admin/adminheader', $vars);
            $this->view($template_name, $vars);
            $this->view('admin/adminfooter', $vars);
        endif;
        
    }
    public function userloginTemplate($template_name, $vars = array(), $return = FALSE)
    {   

        if($return):
            $content  = $this->view('user/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('user/footer', $vars, $return);

            return $content;
        else:
            $this->view('user/header', $vars);
            $this->view($template_name, $vars);
            $this->view('user/footer', $vars);
        endif;
        
    }
    public function userTemplate($template_name, $vars = array(), $return = FALSE)
    {   

        if($return):
            $content  = $this->view('home/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('home/footer', $vars, $return);

            return $content;
        else:
            $this->view('home/header', $vars);
            $this->view($template_name, $vars);
            $this->view('home/footer', $vars);
        endif;
        
    }
    public function Business_partner($template_name, $vars = array(), $return = FALSE)
    {   

        if($return):
            $content  = $this->view('Business_partner/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('Business_partner/footer', $vars, $return);

            return $content;
        else:
            $this->view('Business_partner/header', $vars);
            $this->view($template_name, $vars);
            $this->view('Business_partner/footer', $vars);
        endif;
        
    }
}